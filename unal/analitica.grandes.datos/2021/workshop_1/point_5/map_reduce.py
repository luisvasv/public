
# Ordene el archivo por fecha.

# reference https://docs.python.org/3/howto/sorting.html
import sys
from datetime import datetime

elements = []

def take_element(element):
    return element.split(",")[3]

for row in sys.stdin:
    elements.append(row)
else:
    elements = sorted(elements, key = take_element)
    # elements = sorted(elements, key = lambda data: data.split(",")[3])
    for element in elements:
        sys.stdout.write(element)
