<< 'MULTILINE-COMMENT'
  En linux podemos crear funciones genericas y luego importarlas
  desde otros archivos
MULTILINE-COMMENT

function imprimir_encabezado_consola(){
  
  echo ""
  echo " __    ____  _  _  __  __  _  _" 
  echo "(  )  (_  _)( \( )(  )(  )( \/ )"
  echo " )(__  _)(_  )  (  )(__)(  )  ( "
  echo "(____)(____)(_)\_)(______)(_/\_)"
  echo "------->"
  echo "logged ----------:"
  echo "         date  : $(date +'%d/%m/%Y')"
  echo "         hour  : $(date +'%H:%M:%S')"
  echo "---------------------------------------"
  echo ""
}


function suma(){
  valor1=$1
  valor2=$2
  let suma=$valor1+valor2
  echo $suma
}