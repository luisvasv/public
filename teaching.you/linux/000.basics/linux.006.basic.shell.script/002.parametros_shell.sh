<< 'MULTILINE-COMMENT'
  todos los programas tienen la capacidad de recibir argumentos para su respectiva ejecucion.
  
  eje: bash miprograma.sh pepito perez perez
  nota: programa que recibe 3 parametros
  
  consideraciones:
  
    1. las shells por defecto, tienen una serie de parametros especiales, los cuales ayudan a validar la informacion que se envia como argumento

    2. los parametros de la shell son una lista, pero no empiezan desde cero

    3. los parametros de una shell se acceden con $numero_posicion

    4. el parametro $0, siempre esta reservado para el nombre del programa

    

MULTILINE-COMMENT

# ojo : estos parametros especiales solo aplican dentro de archivos .sh
echo "nombre  ejecutable   : $0" # nombre del archivo
echo "primer  parametro    : $1" # parametro #1
echo "segundo parametro    : $2" # parametro #2
echo "imprimiendo todos v1 : $@" # todos los parametros(array)
echo "imprimiendo todos v1 : $*" # todos los parametros(string)
echo "contando parametros  : $#" # cantidad de parametros enviados


# ejemplo basico de uso de parametros especiales:


# validacion de argumentos
if [ $# -eq 3 ]; then
  # validacion cumplida, entra a realizar una segunda validacion de signo
  echo "proceder..."
  numero_uno=$1 # obteniendo valor del parametro 1
  numero_dos=$2 # obteniendo valor del parametro 2
  signo=$3      # obteniendo valor del parametro 3

  #&& --> sirve para realizar "y|and" logico -> si 1>2 y 2 == 2 haga.. | todas se deben cumplir
  # ||  --> sirve para realizar "o|or" logico -> si 1>2 o 2 == 2 haga..| al menos una se debe cumplir
  if [ "$signo" == "+" ] || [ "$signo" == "-" ]; then
    echo "ok....."
  else
    echo "operador invalido"
    exit 1 # devolviendo el estado de ejecucion de un programa
  fi


  echo "numero 1, asignado --> $numero_uno"
  echo "numero 2, asignado --> $numero_dos"
  echo "signo   , asignado --> $signo"

  echo "numero 1, no asignado --> $1"
  echo "numero 2, no asignado --> $2"
  echo "signo   , no asignado --> $3"

else 
  # si no se le envia todos los argumentos, generara este mensaje de error
  echo "Error......"
  echo ""
  echo "la cantidad de parametros ingresados son invalalidos"
  echo ""
  echo "parametros ingresados  = $#, parametros requeridos = 3"
  echo ""
  echo "[HELP]"
  echo ""
  echo "operaciones permitidas [ + - / *]"
  echo "usange :  bash $0 1 1 +"
  exit 100 # devolviendo el estado de ejecucion de un programa
fi



