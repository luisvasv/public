from code.python.utilities import schema_validation, read_json_file, get_basic_logging, get_schema_from_yaml
import json
import logging

# logger
logger = get_basic_logging()

# 1. simple document
schema = read_json_file("samples/json_schemas/001.simple_validation.json")
document = read_json_file("samples/json_documents/001.simple_validation.json")

# 2. fields validation
# schema = read_json_file("samples/json_schemas/002.login_validation.json")
# document = read_json_file("samples/json_documents/002.login_validation.json")

# 3. object validation
# schema = read_json_file("samples/json_schemas/003.object_validation.json")
# document = read_json_file("samples/json_documents/003.object_validation.json")

# 4. array validation
# schema = read_json_file("samples/json_schemas/004.list_validation.json")
# document = read_json_file("samples/json_documents/004.list_validation.json")

# 5. best practice json
# schema = read_json_file("samples/json_schemas/005.best_practices.json")
# document = read_json_file("samples/json_documents/004.list_validation.json")

# 6. best practice yaml
# schema = get_schema_from_yaml("samples/yaml_schema/001.best_practice.yaml")
# document = read_json_file("samples/json_documents/004.list_validation.json")

# schema validation
response = schema_validation(schema=schema, document=document)

# output
logger.warning("status code : {}".format(response["code"]))
logger.warning("status code : {}".format(response["msm"]))
