Se debe abrir la base de datos :
    
# WORKSHOP - DATA STORAGE

El objetivo de este laboratorio, consiste en exportar información cruda mediante archivos planos. 
 Luego crear una pequeña bodega de datos usando [databricks-community](https://community.cloud.databricks.com/), crear las entidades requeridas, generar los sql para la obtener resultado esperado y luego enviarlo para su respectiva verificación

## Parte 1

Se debe abrir la base de datos [dimensions.db](https://gitlab.com/luisvasv/public/-/blob/master/demos/s3_datalake/data/database/dimensions.db) y exportar a CSV las siguientes tablas:

```bash
country
holidays
```
Luego se deberan crear ambas tablas en databricks y realizar un inner join entre ambas, las tablas deberan quedar almacenada en la base de datos `unal_smdb`

```sql
SELECT ..
FROM ...
INNER JOIN ..
ON .. = ..
```
## Parte 2

Se debe descargar el [dataset](https://gitlab.com/luisvasv/public/-/blob/master/demos/s3_datalake/data/workshop/vehicules.json) de parqueaderos, el cual se encuentra de la siguiente forma:

```python
{
   "mall":"molinos",
   "parking":{
      "in":"27/03/2019",
      "out":"13/07/2020"
   },
   "car_id":"6083714ab540065835731803"
},
{
   "mall":"el tesoro",
   "parking":{
      "in":"09/02/2020",
      "out":"09/02/2021"
   },
   "car_id":"6083714a555ad3a06b134b17"
}
```

Luego se debera hacer un aplanado de los datos, para que queden de la siguiente manera :

```python
mall       car_id                      y_in  m_in   d_in    y_out    m_out   d_out
molinos	   6083714ab540065835731803    2019	  3	     27	     2020	   7	  13
el tesoro  6083714a555ad3a06b134b17    2020	  2	     9	     2021	   2	  9

```

## Parte 3
Los datos aplanados se debenera subir a databricks y crear una talla llamada `parking`, como se realizó en la parte 1, luego usando SQL, se deberan responder las siguientes preguntas de negocio filtrando por el `country=CO`:

- Cuantos sabados y domingos visitaron, resultado esperado :
```sql
| MALL      | DAY     | COUNTER |
|-----------|---------|---------|
| EL TESORO | SABADO  | 1       |
| EL TESORO | DOMINGO | 8       |
| MOLINOS   | SABADO  | 7       |
| MOLINOS   | DOMINGO | 8       |
| SANTAFE   | SABADO  | 3       |
| SANTAFE   | DOMINGO | 10      |
```

- Cual fue el numero de visitas en un mes que paso en por centro comercial, resultado esperando:

```sql
| MALL      | MAX_PER_DAY |
|-----------|-------------|
| MOLINOS   | 97          |
| EL TESORO | 84          |
| SANTAFE   | 72          |
```

- Cuales fueron las visitas totales por centro comercial, resultado esperando:

```sql
| MALL      | COUNTER     |
|-----------|-------------|
| MOLINOS   | 492         |
| EL TESORO | 435         |
| SANTAFE   | 357         |
```

## Entregables

- se debe entregar el script con que aplanaron los datos
- se debe entregar un scrip que contenga los 3 `sql's` que usaron
