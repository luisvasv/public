[![N|Solid](https://media-exp1.licdn.com/dms/image/C4D0BAQHfe6lnbeWn9A/company-logo_100_100/0/1613514339314?e=2159024400&v=beta&t=hS1y6L1LRIqPEsy3y-zVuUtv3DEJ8ZYb-oMpyi3lhBM)](https://aws.amazon.com/es/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=*all)

# **DATA STORAGE USING AWS**
****
Data storage refers to the use of recording media to retain data using computers or other devices. The most prevalent forms of data storage are file storage, block storage, and object storage, with each being ideal for different purposes.


**Advantages**

- Usability and accessibility
- Security
- Cost-efficient
- Convenient sharing of files
- Automation
- Multiple users
- Synchronization
- Convenience
- Scalable
- Disaster recovery

## **prerequisites**
****
- Have python installed
- Have installed MongoDB
- Have an account in Databricks community
- Have and account in AWS
- Have configured AWS on your local machine(CLI)
- Have AWS Console enabled

## **Instructions**
****
1. Install dependencies required
 
    conda commands:
    ```sh
    conda env create -f libraries/demo.yml 
    conda activate demo_s3
    ```
    Pip commands:
    ```sh
    pip install -r demos/requirements.txt
    ```
2. Run code
    
    If you want to run the executable, please execute 
    and then execute : 

    ```sh
    sh components/run.sh
    ```
    
   
## **Notes**
****

* Crawlers needs to be created manually