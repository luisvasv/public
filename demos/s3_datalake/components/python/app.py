import __meta__ as app
from src.parameters import get_args
import src.utils as utils
import pandas as pd

try:
    args = get_args().parse_args()
    if args.engine == "sqlite3":
        # get sqlite3 conexion
        database_conexion = utils.get_sqlite3_connection(app.__database__)
        # get data and load at dataframe
        df = utils.get_table_pandas(database_conexion, "SELECT * FROM {}".format(args.table))
        # save csv
        utils.save_dataframes(df, app.__csv__,args.table ,"csv")
        # save parquet
        utils.save_dataframes(df, app.__parquet__,args.table ,"parquet")
    else:
        # get mongo connection
        mongo_conexion = utils.get_mongo_connection(host='localhost', db=args.database)
        # set dataframe
        df =  pd.DataFrame(list(mongo_conexion[args.table].find({})))
        del df['_id']
        # save json
        utils.save_dataframes(df, app.__json__,args.table ,"json")
except Exception as ex:
    print("[ERROR] -----> \n" + str(ex))
