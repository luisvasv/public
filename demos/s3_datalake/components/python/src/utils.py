import sqlite3
import pandas as pd
import os
from pymongo import MongoClient

def get_sqlite3_connection(path:str):
    """ A util for making a connection to sqlite3 """
    return sqlite3.connect(path)

def get_table_pandas(connexion, sql):
    """ A util for read table using an engine """
    return pd.read_sql_query(sql, connexion)

def save_dataframes(df, path, name, format_type, sep=","):
    """ A util for save the dataframe """
    if format_type == "json":
        df.to_json(os.path.join(path, name + ".json"), orient='records', lines=True)
    elif format_type == "csv":
        df.to_csv(os.path.join(path, name + ".csv"), sep=sep, encoding='utf-8', index=False)
    elif format_type == "excel":
        writer = pd.ExcelWriter(os.path.join(path, name + ".xlsx"))
        df.to_excel(writer, 'info', index=False)
        writer.save()
    elif format_type == "parquet":
        df.to_parquet(os.path.join(path, name + ".parquet"))
    else:
        raise Exception("invalid format") 

def get_mongo_connection(host, port=27017, username=None, password=None, db="default"):
    """ A util for making a connection to mongo """
    if username and password:
        mongo_uri = f'mongodb://{username}:{password}@{host}:{port}/{db}'
        connection = MongoClient(mongo_uri)
    else:
        connection = MongoClient(host, port)
    return connection[db]