#!/bin/bash

# load env variables
source .env
source ./app/linux/utilities/generics.sh


# get engine
engine=$(validate_args $@)
if [ $? -ne 0 ]; then
    echo "$engine"
    exit 1
fi 

# check localstack service
service=$(localstack status | grep -c "running")
if [ "$service" -eq 1 ]; then
    echo "[OK|INFO] - localstack servive is running"
else
    echo "[WARNING|INFO] localstack is not runnig, starting the service."
    localstack start &
    sleep 20
fi

# s3 config
echo "[OK|INFO] - creating S3 buckets..."
if [ "$engine" == "localstack" ]; then
    awslocal --endpoint-url $AWS_CORE_END_POINT s3 mb "s3://"$AWS_S3_BUCKET_RAW 2> /dev/null
else
    aws s3 mb "s3://"$AWS_S3_BUCKET_RAW 2> /dev/null
fi
echo ""

echo "[OK|INFO] - creating queue topic..."

# crear topics 
if [ "$engine" == "localstack" ]; then
    awslocal --endpoint-url $AWS_CORE_END_POINT sqs create-queue --queue-name $AWS_SQS_EVENTS
    awslocal --endpoint-url $AWS_CORE_END_POINT sqs create-queue --queue-name $AWS_SQS_COSTS 
else
    aws sqs create-queue --queue-name $AWS_SQS_EVENTS
    aws sqs create-queue --queue-name $AWS_SQS_COSTS 
fi
echo ""

echo "[OK|INFO] - creating dybamodb table..."
if [ "$engine" == "localstack" ]; then
    awslocal --endpoint-url $AWS_CORE_END_POINT dynamodb create-table \
        --table-name $AWS_DYNAMO_TABLE_NAME \
        --attribute-definitions \
            AttributeName=partition_date,AttributeType=S \
            AttributeName=order_id,AttributeType=S \
        --key-schema \
            AttributeName=partition_date,KeyType=HASH \
            AttributeName=order_id,KeyType=RANGE \
        --provisioned-throughput \
        ReadCapacityUnits=1,WriteCapacityUnits=1 2> /dev/null 
else 
    aws dynamodb create-table \
        --table-name $AWS_DYNAMO_TABLE_NAME \
        --attribute-definitions \
            AttributeName=partition_date,AttributeType=S \
            AttributeName=order_id,AttributeType=S \
        --key-schema \
            AttributeName=partition_date,KeyType=HASH \
            AttributeName=order_id,KeyType=RANGE \
        --provisioned-throughput \
        ReadCapacityUnits=1,WriteCapacityUnits=1  2> /dev/null 
fi
echo ""

echo -e "[OK|INFO] - creating cloudwatch config ...\n"
if [ "$engine" == "localstack" ]; then
    awslocal --endpoint-url $AWS_CORE_END_POINT logs create-log-group --log-group-name $AWS_LOG_GROUP 2> /dev/null
    awslocal --endpoint-url $AWS_CORE_END_POINT logs create-log-stream --log-group-name  $AWS_LOG_GROUP --log-stream-name $AWS_LOG_STREAM_NAME 2> /dev/null
else
    aws logs create-log-group --log-group-name $AWS_LOG_GROUP 2> /dev/null
    aws logs create-log-stream --log-group-name  $AWS_LOG_GROUP --log-stream-name $AWS_LOG_STREAM_NAME 2> /dev/null
fi

# listing services
echo "[OK|INFO] - listing S3 buckets..."
if [ "$engine" == "localstack" ]; then
    echo ""
    awslocal --endpoint-url $AWS_CORE_END_POINT s3 ls
    echo ""
else
    echo ""
    aws s3 ls
    echo ""
fi

echo "[OK|INFO] - listing queue..."
if [ "$engine" == "localstack" ]; then
    echo ""
    awslocal --endpoint-url $AWS_CORE_END_POINT sqs list-queues
    echo ""
else
    echo ""
    aws sqs list-queues
    echo ""
fi

echo "[OK|INFO] - listing dynamo tables..."
echo ""
if [ "$engine" == "localstack" ]; then
    awslocal --endpoint-url $AWS_CORE_END_POINT dynamodb list-tables
    # awslocal --endpoint-url $AWS_CORE_END_POINT \
    #    dynamodb describe-table --table-name $AWS_DYNAMO_TABLE_NAME | grep TableStatus
else
    aws dynamodb list-tables
fi
echo ""


echo "[OK|INFO] - listing cloud watch config..."
echo ""
if [ "$engine" == "localstack" ]; then
    awslocal --endpoint-url $AWS_CORE_END_POINT logs describe-log-groups 
    # awslocal --endpoint-url $AWS_CORE_END_POINT \
    #    dynamodb describe-table --table-name $AWS_DYNAMO_TABLE_NAME | grep TableStatus
else
    aws logs describe-log-groups 
fi
echo ""
