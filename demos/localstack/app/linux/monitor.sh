


#!/bin/bash

# load env variables
source ./app/linux/utilities/generics.sh
source .env

# get engine
engine=$(validate_args $@)
if [ $? -ne 0 ]; then
    echo "$engine"
    exit 1
fi 


if [ "$engine" == "localstack" ]; then
    awslocal --endpoint-url $AWS_CORE_END_POINT logs tail $AWS_LOG_GROUP --follow
else
    aws logs tail $AWS_LOG_GROUP --follow
fi