#!/bin/bash

# load env variables
source ./app/linux/utilities/generics.sh
source .env

# get engine
engine=$(validate_args $@)
if [ $? -ne 0 ]; then
    echo "$engine"
    exit 1
fi 

# request nro events
echo -n "input nro of events: "
read -e records

# validate date format
validate_number $records

# variables
START_DATE=8                    # start work day
END_DATE=17                     # end work day
random_number=$(( RANDOM % 7 )) # nro dates, emulates days

# generate events
for (( day=0; day<$random_number; day++ )); do
    DATE=$(date -j -v-"$day"d +"%d/%m/%Y")
    for ((index = 1; index <= records; index++)); do
        echo "[OK|INFO] - generating message, day: $DATE, # $index ..."
        sleep $day
        message=$(python -m app.python.producer.app --date $DATE --start_date $START_DATE --end_date $END_DATE)
        echo $message
        if [ "$engine" == "localstack" ]; then
            URL=$(awslocal --endpoint-url $AWS_CORE_END_POINT sqs list-queues | jq .QueueUrls  | jq --arg aws_sqs_events "$AWS_SQS_EVENTS" '.[] | select(contains($aws_sqs_events))')
            URL=$(echo "$URL" | sed 's/"//g')
            awslocal --endpoint-url $AWS_CORE_END_POINT sqs --queue-url send-message "$URL" --message-body "${message}"
        else
            URL=$(aws sqs list-queues | jq .QueueUrls  | jq --arg aws_sqs_events "$AWS_SQS_EVENTS" '.[] | select(contains($aws_sqs_events))')
            URL=$(echo "$URL" | sed 's/"//g')
            aws sqs --queue-url send-message "$URL" --message-body "${message}"  --no-cli-pager --output json
        fi
        echo "[OK|INFO] - message # $index  sent to $AWS_SQS_EVENTS..."
    done
done
