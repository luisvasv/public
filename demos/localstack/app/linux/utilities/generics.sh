#!/bin/bash
# TODO convertir de español a ingles
function validate_args(){
    error_message="you must send engine paramater, example --engine=[aws|localstack]"
    if [ $# -eq 1 ]; then
        for arg in "$@"
        do
            case $arg in
                --engine=*)
                    engine="${arg#*=}"
                    ;;
                *)
                    echo "unknow args: $arg"
                    echo $error_message
                    exit 1
                    ;;
            esac
        done
    else
        echo $error_message
        exit 1 
    fi

    # validate args
    if [ -z "$engine" ]; then
        echo $error_message
        exit 1
    fi

    # Validar que el valor de --engine sea localstack o aws
    if [ "$engine" != "localstack" ] && [ "$engine" != "aws" ]; then
        echo "argument value for --engine must be: localstack or aws"
        exit 1
    fi

    echo $engine

}

function validate_number(){
    # checknumber format
    re='^[0-9]+$'
    if ! [[ $1 =~ $re ]]; then
        echo "[ERROR]: input must be a number."
        exit 1
    fi

}