import os
import pandas as pd
from dotenv import load_dotenv
from fastapi import FastAPI, Query
from typing import Any, Dict, List
from app.python.utils.aws import AwsConfig
from fastapi.responses import HTMLResponse
from app.python.utils.generic import Utils
from app.python.utils.enumerators import EngineEnum

# load env variables
load_dotenv()

# define service
app = FastAPI()


@app.get("/get_all_by_date")
async def get_all_by_date(
    date: str = Query(..., description="Date event, format DDMMYYYY"),
    engine: EngineEnum = Query(..., description="Engine type (aws or localstack)"),
    authentication_id: str = Query(..., description="authorization user, coming from auth)")
) -> List[Dict[Any, Any]]:
    """service to get all data from specific date

    :param date: format DDMMYYY, defaults to Query(..., description="Date event, format DDMMYYY")
    :type date: str, optional
    :param engine: aws or localstack
    :type engine: EngineEnum, optional
    :param authentication_id: user id
    :type authentication_id: str, optional
    :return: data retrieved
    :rtype: List[Dict[Any, Any]]
    """
    client = AwsConfig.get_config_dynamodb(engine)
    response: Dict[Any, Any] = AwsConfig.dynamodb_query_partition_key(
        dynamodb=client,
        partition_name="partition_date",
        partition_key_value=date
    )
    elements: List[Dict[Any, Any]] = [Utils.remove_noise_json(item) for item in response['Items']]

    # save info costs
    message_body: str = Utils.get_api_call_info(elements, "get_all_by_date", authentication_id)
    AwsConfig.write_sqs(os.getenv("AWS_SQS_COSTS"), message_body, engine)
    return elements


@app.get("/get_data_by_date_order_id")
async def get_data_by_date_order_id(
    date: str = Query(..., description="Date event, format DDMMYYY"),
    order_id: str = Query(..., description="identifier order id"),
    engine: EngineEnum = Query(..., description="Engine type (aws or localstack)"),
    authentication_id: str = Query(..., description="authorization user, coming from auth)")
) -> Dict[Any, Any]:
    """service to get all data from specific date by order_id

    :param date: format DDMMYYY
    :type date: str, optional
    :param order_id: order id
    :type order_id: str, optional
    :param engine: aws or localstack
    :type engine: EngineEnum, optional
    :param authentication_id: user id
    :type authentication_id: str, optional
    :return: data retrieved
    :rtype: List[Dict[Any, Any]]
    """
    client = AwsConfig.get_config_dynamodb(engine)
    response: Dict[Any, Any] = AwsConfig.dynamodb_query_partition_key_sort_key(
        dynamodb=client,
        partition_name="partition_date",
        partition_key_value=date,
        sort_key_value_name="order_id",
        sort_key_value=order_id
    )
    elements: List[Dict[Any, Any]] = [Utils.remove_noise_json(item) for item in response['Items']]
    # save info costs
    message_body: str = Utils.get_api_call_info(
        elements[0] if len(elements) > 0 else ["no-data"], "get_all_by_date", authentication_id)
    AwsConfig.write_sqs(os.getenv("AWS_SQS_COSTS"), message_body, engine)
    return elements[0] if len(elements) > 0 else ["no-data"]


@app.get("/get_stats_per_day")
async def get_stats_per_day(
    date: str,
    engine: EngineEnum = Query(..., description="Engine type (aws or localstack)"),
    authentication_id: str = Query(..., description="authorization user, coming from auth)")
) -> str:
    """service to get stats by date

    :param date: format DDMMYYY
    :type date: str, optional
    :param engine: _description_, defaults to Query(..., description="Engine type (aws or localstack)")
    :type engine: EngineEnum, optional
    :param authentication_id: _description_, defaults to Query(..., description="authorization user, coming from auth)")
    :type authentication_id: str, optional
    :return: _description_
    :rtype: List[Dict[Any, Any]]
    """
    values: Dict[Any, Any] = {}
    client = AwsConfig.get_config_dynamodb(engine)
    response: Dict[Any, Any] = AwsConfig.dynamodb_query_partition_key(
        dynamodb=client,
        partition_name="partition_date",
        partition_key_value=date
    )
    elements: List[Dict[Any, Any]] = [Utils.remove_noise_json(item) for item in response['Items']]
    if len(elements) > 0:
        df = pd.DataFrame(elements)
        df['quantity_products'] = pd.to_numeric(df['quantity_products'])
        df_agrupado = df.groupby('commune').sum().reset_index()
        values = df_agrupado.to_json(orient='records', index=False)

    # save info costs
    message_body: str = Utils.get_api_call_info(values, "get_stats_per_day", authentication_id)
    AwsConfig.write_sqs(os.getenv("AWS_SQS_COSTS"), message_body, engine)
    return values


@app.get("/", response_class=HTMLResponse)
async def index() -> HTMLResponse:
    """get api page welcome

    :return: HTMLResponse
    :rtype: HTMLResponse
    """
    # save info costs
    # message_body: str = Utils.get_api_call_info("", "index", "index")
    # AwsConfig.write_sqs(os.getenv("AWS_SQS_COSTS"), message_body, "no-required")
    return HTMLResponse(content=Utils.get_index_html(), status_code=200)
