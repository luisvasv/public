
import dash_bootstrap_components as dbc
from dash import dcc, html


def get_dashboard_layout(area_graph, engine: str) -> dbc:
    """get layer for the dashboard app

    :param area_graph: ploty object
    :return: dash bootstrap component layout
    :rtype: dbc
    """
    ccs_1: str = "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    logo_1: str = "https://upload.wikimedia.org/wikipedia/commons/d/da/SoftServe_logo_new.png"
    logo_2: str = "https://2024.pycon.co/images/pycon-logo.svg"
    cn_1: str = "header navbar navbar-expand-lg navbar-light bg-light"
    js_1: str = "https://code.jquery.com/jquery-3.5.1.slim.min.js"
    js_2: str = "https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
    js_3: str = "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    return dbc.Container(
        [
            html.Div(
                children=[
                    html.Title(children=["PyCon 2024"]),
                    html.Link(
                        rel="stylesheet",
                        href=ccs_1,
                    ),
                    html.Div(
                        className="container",
                        children=[
                            html.Header(
                                className=cn_1,
                                children=[
                                    html.Img(
                                        src=logo_1,
                                        width="150",
                                        className="img-fluid",
                                    ),
                                    html.Img(
                                        src=logo_2,
                                        width="150",
                                        className="img-fluid ml-auto",
                                    ),
                                ],
                            ),
                            html.Section(
                                className="main",
                                children=[
                                    html.Div(
                                        style={
                                            "font-family": "Arial, sans-serif",
                                            "background-color": "#f0f0f0",
                                            "padding": "20px",
                                            "border-radius": "10px",
                                            "text-align": "center",
                                        },
                                        children=[
                                            html.H2(
                                                style={"color": "#333"},
                                                children=[
                                                    "Welcome to our talk: LocalStack & Python Boto3!"
                                                ],
                                            ),
                                            html.P(
                                                style={"color": "#666"},
                                                children=[
                                                    "LangChaing: Localstack + plotty + dash + Open IA 4.0"
                                                ],
                                            ),
                                            html.P(
                                                style={"color": "#666"},
                                                children=[
                                                    f"< ENGINE: {engine} >"
                                                ],
                                            ),
                                        ],
                                    ),
                                ],
                            ),
                        ],
                    ),
                    html.Script(src=js_1),
                    html.Script(
                        src=js_2
                    ),
                    html.Script(
                        src=js_3
                    ),
                ]
            ),
            dbc.Row(
                [
                    dbc.Col(
                        [
                            html.Br(),
                            html.Div(
                                dcc.Graph(
                                    id='area-graph',
                                    figure=area_graph,
                                    style={'width': '1080px', 'margin': 'auto'}
                                ),
                                style={'display': 'flex', 'justify-content': 'center'}
                            ),
                            # dcc.Graph(id='area-graph', figure=area_graph),
                            # dcc.Dropdown(id="domain-slct",
                            #                multi=False,
                            #                options=["aws", "localstack"],
                            #                value=['Multimodal', 'Language', 'Games']),
                            # html.Div("INSIGNTS.....")
                        ],
                        width=6
                    )
                ],
                justify="center"
            ),
            dbc.Row(
                [
                    dbc.Col(dbc.Button(id='btn', children='Insights', className='my-2'), width=1)
                ],
                justify="center"
            ),
            dbc.Row(
                [
                    dbc.Col(dbc.Spinner(html.Div(id='content', children=''), fullscreen=False), width=12)
                ],
            ),
        ]
    )
