
import sys
import argparse
from datetime import datetime


def parse_args_producer() -> argparse.ArgumentParser:
    """argsparse for PRODUCER

    :return: argparse config
    :rtype: argparse.ArgumentParser
    """
    def check_date(date: str):
        try:
            datetime.strptime(date, "%d/%m/%Y")
            return date
        except argparse.ArgumentError:
            argparse.ArgumentTypeError("[ERROR]: invalid date format. Allowed DD/MM/YYYY.")

    parser = argparse.ArgumentParser(description="random data script validator.")
    if len(sys.argv) <= 1:
        parser.print_help()
        sys.exit()
    parser.add_argument("--date", type=check_date, help="date, format: DD/MM/YYYY")
    parser.add_argument("--start_date", type=int, help="start date (numeric)")
    parser.add_argument("--end_date", type=int, help="end date (numeric)")
    return parser


def parse_args_etl() -> argparse.ArgumentParser:
    """argsparse for ETL

    :return: argparse config
    :rtype: argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser(description="code to read sqs and do etl.")
    if len(sys.argv) <= 1:
        parser.print_help()
        sys.exit()
    parser.add_argument("--engine", type=str, choices=["aws", "localstack"], help="define engine to save data")
    return parser


def parse_args_dashboard() -> argparse.ArgumentParser:
    """argsparse for DASHBOARD

    :return: argparse config
    :rtype: argparse.ArgumentParser
    """
    parser = argparse.ArgumentParser(description="code to generate daashboard with langChaing")
    if len(sys.argv) <= 1:
        parser.print_help()
        sys.exit()
    parser.add_argument("--engine", type=str, choices=["aws", "localstack"], help="define engine to save data")
    parser.add_argument("--port", type=int, help="define port dashboard")
    return parser
