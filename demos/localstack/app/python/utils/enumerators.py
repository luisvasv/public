from enum import Enum


class EngineEnum(str, Enum):
    """enumerator to validate API engine
    """
    aws = "aws"
    localstack = "localstack"
