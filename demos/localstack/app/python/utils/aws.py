import os
import json
import boto3
import requests
from dotenv import load_dotenv
from typing import Any, Dict, Tuple

load_dotenv()


class AwsConfig:

    @staticmethod
    def get_config_sqs(engine: str) -> boto3.client:
        """allos to get SQS client

        :param engine: aws or localstack
        :type engine: str
        :return: boto3 configured client for the specified service
        :rtype: boto3.client
        """
        if engine == "localstack":
            return boto3.client(
                service_name='sqs',
                aws_access_key_id='test',
                aws_secret_access_key='test',
                endpoint_url=os.getenv("AWS_CORE_END_POINT"),
            )
        else:
            return boto3.client(
                service_name='sqs'
            )

    @staticmethod
    def get_config_s3(engine: str) -> boto3.client:
        """allos to get S3 client

        :param engine: aws or localstack
        :type engine: str
        :return: boto3 configured client for the specified service
        :rtype: boto3.client
        """
        if engine == "localstack":
            return boto3.client(
                service_name='s3',
                aws_access_key_id='test',
                aws_secret_access_key='test',
                endpoint_url=os.getenv("AWS_CORE_END_POINT")
            )
        else:
            return boto3.client(
                service_name='s3'
            )

    @staticmethod
    def get_config_dynamodb(engine: str) -> boto3.client:
        """allos to get DYNAMODB client

        :param engine: aws or localstack
        :type engine: str
        :return: boto3 configured client for the specified service
        :rtype: boto3.client
        """
        if engine == "localstack":
            return boto3.client(
                service_name='dynamodb',
                aws_access_key_id='test',
                aws_secret_access_key='test',
                endpoint_url=os.getenv("AWS_CORE_END_POINT"),
            )
        else:
            return boto3.client(
                service_name='dynamodb'
            )

    @staticmethod
    def get_config_dynamodb_resource(engine: str) -> boto3.client:
        """allos to get SYNAMODB RESOURCE client

        :param engine: aws or localstack
        :type engine: str
        :return: boto3 configured client for the specified service
        :rtype: boto3.client
        """
        if engine == "localstack":
            return boto3.resource(
                service_name='dynamodb',
                aws_access_key_id='test',
                aws_secret_access_key='test',
                endpoint_url=os.getenv("AWS_CORE_END_POINT"),
            )
        else:
            return boto3.resource(
                service_name='dynamodb'
            )

    @staticmethod
    def dynamodb_query_partition_key(
        dynamodb: boto3.client,
        partition_name: str,
        partition_key_value: str,
        table_name: str = "orders"
    ) -> Dict[Any, Any]:
        """function to execute query on Dynamodb based on partition key

        :param dynamodb: dynamodb client
        :type dynamodb: boto3.client
        :param partition_name: partition name
        :type partition_name: str
        :param partition_key_value: partition key
        :type partition_key_value: str
        :param table_name: table name, defaults to "orders"
        :type table_name: str, optional
        :return: data retrieved from dynamodb
        :rtype: Dict[Any, Any]
        """
        return dynamodb.query(
            TableName=table_name,
            KeyConditionExpression=f'{partition_name} = :val',
            ExpressionAttributeValues={
                ':val': {'S': partition_key_value}
            }
        )

    @staticmethod
    def dynamodb_query_partition_key_sort_key(
        dynamodb: boto3.client,
        partition_name: str,
        partition_key_value: str,
        sort_key_value_name: str,
        sort_key_value: str,
        table_name: str = "orders"
    ) -> Dict[Any, Any]:
        """function to execute query on Dynamodb based on partition key and sort key

        :param dynamodb: dynamodb client
        :type dynamodb: boto3.client
        :param partition_name: partition name
        :type partition_name: str
        :param partition_key_value: partition key
        :type partition_key_value: str
        :param sort_key_value_name: sort key name
        :type sort_key_value_name: str
        :param sort_key_value: sort key value
        :type sort_key_value: str
        :param table_name: table name, defaults to "orders"
        :type table_name: str, optional
        :return: data retrieved from dynamodb
        :rtype: Dict[Any, Any]
        """

        return dynamodb.query(
            TableName=table_name,
            KeyConditionExpression=f'{partition_name} = :val_1 AND {sort_key_value_name} = :val_2',
            ExpressionAttributeValues={
                ':val_1': {'S': partition_key_value},
                ':val_2': {'S': sort_key_value},
            }
        )

    @staticmethod
    def get_config_logs(engine: str) -> boto3.client:
        """allos to get CLOUDWATCH client

        :param engine: aws or localstack
        :type engine: str
        :return: boto3 configured client for the specified service
        :rtype: boto3.client
        """
        if engine == "localstack":
            return boto3.client(
                'logs',
                aws_access_key_id='test',
                aws_secret_access_key='test',
                endpoint_url=os.getenv("AWS_CORE_END_POINT"),
            )
        else:
            return boto3.client('logs')

    @staticmethod
    def get_queue_url(queue_name: str, engine: str) -> Tuple[Any, str]:
        """function to get SQS url

        :param queue_name: queue name
        :type queue_name: str
        :param engine: engine type: aws or localstack
        :type engine: str
        :return: boto3 client and SQS url
        :rtype: Tuple[Any, str]
        """
        sqs: boto3.client = AwsConfig.get_config_sqs(engine)
        return sqs, sqs.get_queue_url(
            QueueName=queue_name
        )['QueueUrl']

    @staticmethod
    def write_sqs(queue_name: str, message_body: str, engine: str) -> None:
        """function to write info into aws SQS

        :param queue_name: queue name
        :type queue_name: str
        :param message_body: message to be sending to SQS
        :type message_body: str
        :param engine: engine type: aws or localstack
        :type engine: str
        """
        sqs, queue_url = AwsConfig.get_queue_url(queue_name, engine)
        sqs.send_message(
            QueueUrl=queue_url,
            MessageBody=message_body
        )

    @staticmethod
    def call_api_stats(date: str, engine: str, authentication_id: str) -> Dict[str, str]:
        """function to call api

        :param date: date format DDMMYYY, it's partition key on DynamoDB
        :type date: str
        :param engine: engine type: aws or localstack
        :type engine: str
        :param authentication_id: user autorization id
        :type authentication_id: str
        :return: API response
        :rtype: Dict[str, str]
        """
        params: Dict[str, str] = {
            "date": date,
            "engine": engine,
            "authentication_id": authentication_id
        }
        response = requests.get(os.getenv("API_STATS_URL"), params=params)
        if response.status_code == 200:
            return json.loads(response.json())

        raise Exception(f"error code {response.status_code}, error: {response.text}")
