import sys
import json
import base64
import random
from shapely.geometry import Point
from typing import Any, Dict, Tuple


class Utils:

    @staticmethod
    def get_index_html() -> str:
        """
        get API welcome content
        """
        html_content = """
                <!DOCTYPE html>
            <html>
            <head>
                <title>API de PyCon 2024</title>
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
                <style>
                    body {
                        font-family: Arial, sans-serif;
                        background-color: #ffffff; /* Cambiado a blanco */
                        text-align: center;
                    }
                    h1 {
                        color: #333;
                    }
                    p {
                        color: #666;
                    }
                </style>
            </head>
            <body>
                <div class="container">
                    <header class="header navbar navbar-expand-lg navbar-light bg-light">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/d/da/SoftServe_logo_new.png"
                        width="150" class="img-fluid">
                        <img src="https://2024.pycon.co/images/pycon-logo.svg"  width="150"
                        class="img-fluid ml-auto">
                    </header>
                    <section class="main">
                        <div style="font-family: Arial, sans-serif; background-color: #f0f0f0; padding: 20px;
                        border-radius: 10px; text-align: center;">
                            <h2 style="color: #333;">Welcome to our talk: LocalStack & Python Boto3!</h2>
                            <p style="color: #666;">We're excited to showcase our application's capabilities using
                            the power of LocalStack
                            and Python. Let's innovate and build amazing things together!</p>
                        </div>
                        <img src="https://miro.medium.com/v2/resize:fit:1400/format:webp/1*HWes1G27skHQgTY4ZsIEFQ.png"
                        alt="PyCon 2024 Logo" width="900">
                    </section>
                    <footer class="footer mt-auto py-3 text-muted">
                    <p></p>
                    <p></p>
                    <p></p>
                        <img src="https://upload.wikimedia.org/wikipedia/commons/d/da/SoftServe_logo_new.png"
                        width="150" class="img-fluid">
                    </footer>
                </div>
                <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
            </body>
            </html>
    """
        return html_content

    @staticmethod
    def remove_noise_json(info: Dict[Any, Any]) -> Dict[Any, Any]:
        """remove noise from responses that coming from dynamodb

        :param info: json that comming from Dynamodb
        :type info: Dict[Any, Any]
        :return: json without dynamo info
        :rtype: Dict[Any, Any]
        """
        tmp: Dict[Any, Any] = {}
        for key, value in info.items():
            if isinstance(value, dict) and len(value) == 1:
                tmp[key] = list(value.values())[0]
            else:
                tmp[key] = value
        return tmp

    @staticmethod
    def get_size_kb_mb(obj_size: Any) -> Tuple[float, float]:
        """get objet size in kilobytes and megabytes

        :param obj_size: data to be evaluated
        :type obj_size: Any
        :return: kb size and  mb size
        :rtype: Tuple[float, float]
        """
        kb_size = obj_size / 1024
        mb_size = kb_size / 1024
        return kb_size, mb_size

    @staticmethod
    def random_lat_lon_geometry(geometry) -> Tuple[float, float]:
        """get latitude and logituded random data based on polygon

        :param geometry: polygon with the geometry
        :type geometry: _type_
        :return: random latitude and longitude values
        :rtype: Tuple[float, float]
        """
        minx, miny, maxx, maxy = geometry.bounds
        while True:
            latitude = random.uniform(miny, maxy)
            longitude = random.uniform(minx, maxx)
            point = Point(longitude, latitude)
            if geometry.contains(point):
                return latitude, longitude

    @staticmethod
    def get_api_call_info(data: Any, api_service: str, authentication_id: str) -> str:
        """get message with costs asociated with API calls
        :param data: data to be evaluated
        :type data: Any
        :param api_service: called service
        :type api_service: str
        :param authentication_id: security api user
        :type authentication_id: str
        :return: _description_
        :rtype: str
        """
        kb_size, mb_size = Utils.get_size_kb_mb(sys.getsizeof(data))
        return json.dumps(
            {
                "authentication_id": authentication_id,
                "kb_size": kb_size,
                "mb_size": mb_size,
                "api_service": api_service,
                "size_len": len(data),
                "data_type": Utils.get_data_type(data)
            }
        )

    @staticmethod
    def get_data_type(data: Any) -> Any:
        """get data type
        :param data: data to evaluate
        :type data: Any
        :return: data type description
        :rtype: Any
        """
        return type(data).__name__

    def encode_image(image_path: str) -> None:
        """function for decoding graph image

        :param image_path: image path location
        :type image_path: str
        :return: N/A
        :rtype:  N/A
        """
        with open(image_path, "rb") as image_file:
            return base64.b64encode(image_file.read()).decode('utf-8')
