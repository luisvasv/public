

import io
import os
import json
import time
import pandas as pd
import geopandas as gpd
from typing import Any, Dict
from datetime import datetime
from dotenv import load_dotenv
from shapely.geometry import Point
from app.python.utils.aws import AwsConfig
from app.python.utils.args import parse_args_etl


load_dotenv()


class App:
    def __init__(self, engine: str) -> None:
        self._sqs = AwsConfig.get_config_sqs(engine)
        self._s3 = AwsConfig.get_config_s3(engine)
        self._dynamodb = AwsConfig.get_config_dynamodb_resource(engine)
        self._logs = AwsConfig.get_config_logs(engine)
        self.__engine: str = engine

    def __save_log(self, message: str) -> None:
        """save cloudwatch log for debbugin

        :param message: log message to be send
        :type message: str
        """
        tmp: Dict[str, Any] = {
            'timestamp': int(time.time() * 1000),  # milisecons
            'message': message
        }
        self._logs.put_log_events(
            logGroupName=os.getenv("AWS_LOG_GROUP"),
            logStreamName=os.getenv("AWS_LOG_STREAM_NAME"),
            logEvents=[tmp]
        )

    def __save_dynamodb(self, df: pd.DataFrame) -> None:
        """save transformed data from SQS to DYNAMODB

        :param df: computed df
        :type df: pd.DataFrame
        """
        table = self._dynamodb.Table(os.getenv("AWS_DYNAMO_TABLE_NAME"))
        item = (df.to_dict(orient='records'))[0]
        response = table.put_item(Item=item)
        self.__save_log(
            "order id {} [STEP|#3] stored data on DybamoDB, table: {}, response : {}".format(
                df['order_id'],
                os.getenv("AWS_DYNAMO_TABLE_NAME"),
                response
            )
        )

    def __save_data_s3(self, df: pd.DataFrame) -> None:
        """save transformed data from SQS to S3

        :param df: computed df
        :type df: pd.DataFrame
        """
        current_date = datetime.now()
        bucket: str = os.getenv("AWS_S3_BUCKET_RAW")
        destination_key: str = "orders/{}/{}_orders.parquet".format(
            current_date.strftime("%d%m%Y"),
            current_date.strftime("%d%m%Y%H%M%S")
        )
        bytes_io = io.BytesIO()
        df.to_parquet(bytes_io, index=False)
        bytes_io.seek(0)
        self._s3.put_object(Bucket=bucket, Key=destination_key, Body=bytes_io)
        self.__save_log(
            f"order id {df['order_id']} [STEP|#4] file saved on S3, path: s3://{bucket}/{destination_key}"
        )

    def __analize_message(self, message: str, neighborhoods: gpd.GeoDataFrame) -> None:
        """Transformation function to transform SQS message

        :param message: message from SQS to be proccessed
        :type message: str
        :param neighborhoods: _description_
        :type neighborhoods: gpd.GeoDataFrame
        """
        neighborhood: str = ""
        commune: str = ""
        df = pd.DataFrame([json.loads(message)])
        df['date'] = pd.to_datetime(df['date'], format='%d/%m/%Y %H:%M:%S').dt.tz_localize(None)
        df['event_year'] = pd.DatetimeIndex(df['date']).year
        df['event_month'] = pd.DatetimeIndex(df['date']).month
        df['event_day'] = pd.DatetimeIndex(df['date']).day
        df['event_hour'] = pd.DatetimeIndex(df['date']).hour
        df['event_minute'] = pd.DatetimeIndex(df['date']).minute
        df['event_second'] = pd.DatetimeIndex(df['date']).second
        df['event_date'] = df['date'].dt.strftime('%d/%m/%Y %H:%M:%S')
        df['employee_id'] = df['employee_id'].astype(int)
        df['customer_id'] = df['customer_id'].astype(int)
        del df['date']

        # getting data
        point = Point(df['longitude'], df['latitude'])
        for _, polygon in gdf.iterrows():
            if point.within(polygon.geometry):
                neighborhood = polygon["NOMBRE"]
                commune = polygon["IDENTIFICACION"]
                break
        df['neighborhood'] = neighborhood
        df['commune'] = commune
        df['longitude'] = df['longitude'].astype(str)
        df['latitude'] = df['latitude'].astype(str)
        df['partition_date'] = datetime.now().strftime("%d%m%Y")
        self.__save_log(f" order id {df['order_id']} --[STEP|#2] ETL done")

        # save df on dynamo
        self.__save_dynamodb(df)
        # save data on s3
        self.__save_data_s3(df)

    def __read_messages(self, neighborhoods: gpd.GeoDataFrame) -> None:
        """read messages from SQS

        :param neighborhoods: polygon data to get neighborhoods
        :type neighborhoods: gpd.GeoDataFrame
        """
        queue_url: str = AwsConfig.get_queue_url(os.getenv("AWS_SQS_EVENTS"), self.__engine)[1]
        # get messages
        response = self._sqs.receive_message(
            QueueUrl=queue_url,
            MaxNumberOfMessages=10,  # max messages to receive
            WaitTimeSeconds=20  # wait time to receive message
        )

        # check if there are message
        if 'Messages' in response:
            for message in [message for message in response['Messages'] if len(message['Body']) > 0]:
                # process message
                self.__analize_message(message['Body'], neighborhoods)
                receipt_handle = message['ReceiptHandle']
                self._sqs.delete_message(
                    QueueUrl=queue_url,
                    ReceiptHandle=receipt_handle
                )

                # save log
                self.__save_log(f"message processed successfully : --> {message['Body']}")
        else:
            self.__save_log("there is no message into queue")

    def run(self, neighborhoods: gpd.GeoDataFrame) -> None:
        """process entry point

        :param neighborhoods: polygon data to get neighborhoods
        :type neighborhoods: gpd.GeoDataFrame
        """
        try:
            while True:
                self.__read_messages(neighborhoods)
        except KeyboardInterrupt:
            print("interrumped process by user...")
            self.__save_log("interrumped process by user...")


if __name__ == "__main__":
    # validate engine
    args = parse_args_etl().parse_args()
    # read neighborhoods data
    gdf = gpd.read_parquet(".localdata/base.data/medellin_neighborhoods.parquet")
    app = App(args.engine)
    # run main process
    app.run(gdf)
