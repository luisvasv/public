import time
import argparse
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
from dotenv import load_dotenv
from datetime import datetime
from langchain_openai import ChatOpenAI
from app.python.utils.generic import Utils
from app.python.utils.aws import AwsConfig
from langchain_core.messages import HumanMessage
from dash import Dash, Input, Output, State, callback
from app.python.utils.args import parse_args_dashboard
from app.python.utils.dashboard import get_dashboard_layout

load_dotenv()
app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])


def dashboard(args: argparse) -> None:
    # read data from API
    current_date: str = datetime.now().strftime("%d%m%Y")
    df: pd.DataFrame = pd.DataFrame(AwsConfig.call_api_stats(current_date, args.engine, "12345"))
    if len(df) > 0:
        # build the line chart
        area_graph = px.area(
            df,
            x='commune',
            y='quantity_products',
            color='commune',
            template='plotly_dark',
            labels={'commune': 'Medellin Districts', 'quantity_products': 'Products Quantity'}
        )
        area_graph.update_layout(legend_title="Quantity")
        area_graph.update_layout(
            width=1080,
            title=dict(
                text="Districts of Medellín with the highest/most sales",
                x=0.5
            )
        )

        # get dashboard layout
        app.layout = get_dashboard_layout(area_graph, args.engine)


@callback(
    Output('content', 'children'),
    Input('btn', 'n_clicks'),
    State('area-graph', 'figure'),
    prevent_initial_call=True,
    suppress_callback_exceptions=True
)
def graph_insights(_, fig):
    message: str = """
    Could you provide me with any relevant information or analysis based on the data I'm visualizing?
    I would appreciate any trends, recomendations or patterns you can identify.
    """
    fig_object = go.Figure(fig)
    fig_object.write_image(f"/tmp/fig{_}.png")
    time.sleep(1)

    chat = ChatOpenAI(model="gpt-4o", max_tokens=256)
    image_path = f"fig{_}.png"
    base64_image = Utils.encode_image(image_path)
    result = chat.invoke(
        [
            HumanMessage(
                content=[
                    {"type": "text", "text": message},
                    {
                        "type": "image_url",
                        "image_url": {
                            "url": f"data:image/jpeg;base64,{base64_image}",
                            "detail": "auto",
                        },
                    },
                ]
            )
        ]
    )
    return result.content


if __name__ == '__main__':
    args = parse_args_dashboard().parse_args()
    dashboard(args)
    app.run_server(debug=True, port=args.port)
