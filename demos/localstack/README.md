<div style="display: flex; justify-content: space-around; align-items: center; width: 90%;">
  <img src="https://neuronamagazine.com/wp-content/uploads//2022/07/SoftServe_logo_new-681x205.png" alt="SoftServe Logo" style="height: 60px;">
  <img src="https://2024.pycon.co/images/pycon-logo.svg" alt="PyCon 2024 Logo" style="height: 30px;">
</div>


<img 
     src="https://i.postimg.cc/kgZ4w683/localst.png" 
     alt="reporte" 
     border="0"
/>

## **TLDR**

LocalStack aims to emulate AWS cloud services, allowing to develop and test in a local terminal.

<img 
     src="https://miro.medium.com/v2/resize:fit:1400/1*HWes1G27skHQgTY4ZsIEFQ.png" 
     alt="reporte" 
     border="0"
/>



<div class="alert alert-warning">
 <strong>⚠️IMPORTANT:</strong> To replicate this tutorial by yourself, please follow all the steps in the next section.
</div>

# SET UP

## [001] SET UP VSCODE
VSCode (Visual Studio Code) is a free, open-source code editor developed by Microsoft, exalted for its support for debugging, syntax higlights, smart code completion, snippets, and Git integration.

[Download VSCODE](https://code.visualstudio.com/download)

Once VSCode have been downloaded, configure it as follows

Go to the Extensions section and install Tasks Exlorer.

<img 
     src="https://i.postimg.cc/V6K2BMrX/tasks.png" 
     alt="reporte" 
     border="0"
/>

After it is installed, go to Extensions > Tasks Exlorer > Extensions Settings > Enable Side Bar 
<img 
src="https://i.postimg.cc/05JLJ65K/config.png" 
     alt="reporte" 
     border="0"
/>

<img 
src="https://i.postimg.cc/xdCZj8FC/enabled.png" 
     alt="reporte" 
     border="0"
/>

Restart VSCODE, click on the Task Explorer icon and it should appear like this:

<img 
src="https://i.postimg.cc/C5g2p9cw/tasks-2.png" 
     alt="reporte" 
     border="0"
/>


## [002] SET UP AWS (OPTIONAL)
This demo allows direct interaction with AWS. If you also want to do this, you need to have `aws_access_key_id` and `aws_secret_access_key` configured locally. If you don't have an AWS account or don't know how to generate these values, you can watch the following videos:

- [How to sign up for the AWS free tier](https://www.youtube.com/watch?v=SFaSB6vgp8k&ab_channel=CloudTech)
- [How to generate Access and Secret Key on AWS](https://www.youtube.com/watch?v=HuE-QhrmE1c&ab_channel=AOSNote)


## [003] INSTALL PYTHON LIBRARIES

Once the environment is already setup, do the following:


### [003.1] Run:
```bash
# Make sure you're working in Python version 3.10.x, then install the poetry environment
python -m poetry install 

# Note: If you have troubles with the dependencies, run within the poetry environment
pip install --force-reinstall --no-cache --upgrade pip dash-bootstrap-components langchain-core langchain-openai plotly-express fastapi requests kaleido localstack awscli-local

```
### [003.2] Check localstack status
Using the task explorer:
- Run the task under the name: ``[API].intro.localstack``

Using the terminal:
```bash
bash ./app/linux/le-servicesinto.sh
```

This will retrieve the status of the services available in localstack suite.
```bash
  _                 _     _             _ 
 | | ___   ___ __ _| |___| |_ __ _  ___| | __
 | |/ _ \ / __/ _  | / __| __/ _  |/ __| |/ /
 | | (_) | (_| (_| | \__ \ || (_| | (__|   < 
 |_|\___/ \___\__,_|_|___/\__\__,_|\___|_|\_\ 

┏━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━┓
┃ Service                  ┃ Status      ┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━┩
│ acm                      │ ✔ available │
│ apigateway               │ ✔ available │
│ cloudformation           │ ✔ available │
│ cloudwatch               │ ✔ available │
│ config                   │ ✔ available │
│ dynamodb                 │ ✔ available │
│ dynamodbstreams          │ ✔ available │
│ ec2                      │ ✔ available │
│ es                       │ ✔ available │
│ events                   │ ✔ available │
│ firehose                 │ ✔ available │
│ iam                      │ ✔ available │
│ kinesis                  │ ✔ available │
│ kms                      │ ✔ available │
│ lambda                   │ ✔ available │
│ logs                     │ ✔ available │
│ opensearch               │ ✔ available │
│ redshift                 │ ✔ available │
│ resource-groups          │ ✔ available │
│ resourcegroupstaggingapi │ ✔ available │
│ route53                  │ ✔ available │
│ route53resolver          │ ✔ available │
│ s3                       │ ✔ available │
│ s3control                │ ✔ available │
│ scheduler                │ ✔ available │
│ secretsmanager           │ ✔ available │
│ ses                      │ ✔ available │
│ sns                      │ ✔ available │
│ sqs                      │ ✔ available │
│ ssm                      │ ✔ available │
│ stepfunctions            │ ✔ available │
│ sts                      │ ✔ available │
│ support                  │ ✔ available │
│ swf                      │ ✔ available │
│ transcribe               │ ✔ available │
└──────────────────────────┴─────────────┘
```
## [004] START DEMO

From this point, you will be able to run the demo in stages.

### [004.1] DEPLOY LOCALSTACK/AWS SERVICES AND RESOURCES

This section allows the provisioning of the AWS services required for subsequent processes.



Using the task explorer:
- For localstack Run the task under the name: ``[localstack|001].deploy.services``
- For AWS Run the task under the name: ``[aws|001].deploy.services``

Using the terminal:
```bash
bash app/linux/init.sh --engine=[localstack|aws]
```
This will deploy the necessary components either locally or in the cloud. 
- **S3 Bucket** to store the events as Parquet files.
- **DynamoDB** table to the store the events in a NoSQL database.
- **SQS Queue**, which will be the delivery channel for receiving and transporting the events to the data pipeline.
- **CloudWatch** logs for monitoring the cloud and data components.

**NOTE:** Check the status of the services and resources using the tasks/commands from the previous step.

### [004.2] GENERATE GEOLOCATION EVENTS

This section will simulate events.

Using the task explorer:
- For localstack Run the task under the name: ``[localstack|002].simulate.real.time``
- For AWS Run the task under the name: ``[aws|002].simulate.real.time``

Using the terminal:
```bash
bash app/linux/producer.sh --engine=[localstack|aws]
```

This utilizes a Python client for randomly producing a requested number of JSON events and send them into the SQS Queue.

### [004.3] RUN ETL TO CONSUME AND TRANSFORM 


This section allows data transformation processes and distribution across different services.


Using the task explorer:
- For localstack Run the task under the name: ``[localstack|003].run.etl.service``
- For AWS Run the task under the name: ``[aws|003].run.etl.service``

Using the terminal:
```bash
python -m app.python.etl.app --engine [localstack|aws]
```

The python job fetches the messages living in the SQS Queue and transform them according to the requirements of the lab. The Job acts as a listener, meaning that it'll enter into an infinite loop and will retrieve every message once its available in the SQS Queue.

### [004.4] MONITOR THE CHANNEL, CONSUMRE AND PRODUCER


This section enables the monitor to validate the messages generated by the apps.


Using the task explorer:
- For localstack Run the task under the name: ``[localstack|004].monitoring.cloudwatch``
- For AWS Run the task under the name: ``[aws|004].monitoring.cloudwatch``

Using the terminal:
```bash
bash app/linux/monitor.sh --engine=localstack | aws
```

The script fetches the cloudwatch logs to check if there a failure related to the SQS Queue and message production/delivery.

### [004.5] RUN API ENDPOINT

This section allows running the service to have the data consumption and productization API.

Using the task explorer:
- For localstack Run the task under the name: ``[API].run.api.service``

Using the terminal:
```bash
uvicorn app.python.api.app:app --reload
```

This step makes online the API endpoint and forwards it to the address ``http://127.0.0.1:8000`` for welcome API

The swagger will be available at ``http://127.0.0.1:8000/docs`` in this link you may test the service.

### [004.6] PUBLISH THE REPORT/DASHBOARDP

This section allows generating the dashboard and running LangChaing to interpret the graph using artificial intelligence.

Using the task explorer:
- For localstack Run the task under the name: ``[localstack|005].dashboard``
- For AWS Run the task under the name: ``[aws|005].dashboard``

Using the terminal:
```bash
# to use AWS data
python -m app.python.dashboard.app --engine localstack | aws --port 7000
```

```bash
# to use Localstack data
python -m app.python.dashboard.app --engine localstack | aws --port 9000
```

The python module will enable the API endpoint for accessing the dashboard in the address ``http://127.0.0.1:7000`` or ``http://127.0.0.1:9000`` depending the case

![image](https://hackmd.io/_uploads/H1-4GAySA.png)

<div class="alert alert-warning">
    <strong>⚠️IMPORTANT:</strong> Make sure you have a valid token to the API of OpenAI to enable the usage of GPT 4.0o within the lab. The token can be set in the environment variable OPENAI_API_KEY within the .env file 
</div>

To perform the analysis using ChatGPT-4o, please press the `Insights` button.