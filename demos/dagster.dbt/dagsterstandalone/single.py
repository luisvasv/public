from typing import List
from dagster import asset, AssetExecutionContext, Definitions, define_asset_job, ScheduleDefinition

@asset(group_name="initial", compute_kind="python")
def first_asset():
    """
    first assert
    """
    print("validating and return..")
    return [1,2,3]


@asset(group_name="grouped", compute_kind="aws")
def second_asset(context: AssetExecutionContext):
    """
    validating another one ...
    etc
    abc
    """
    data = [1,2,3]
    context.log.info(f"using log context {data}")
    return data


@asset(group_name="grouped", compute_kind="pandas", deps=[first_asset, second_asset])
def third_asset(context: AssetExecutionContext):
    """
    devolviendo valores en el padre
    """
    data = [9 ,8,7]
    context.log.info(f"testing asset context {data}")
    context.log.info(f"printing--> {third_asset}")
    return data


defs = Definitions(
    assets=[first_asset, second_asset, third_asset],
    jobs=[
        define_asset_job(
            name="hello_dagster_job",
            selection=[first_asset, second_asset, third_asset],
        )
    ],
    schedules=[
        ScheduleDefinition(
            name="hello_demo_dagster_scheduled",
            job_name="hello_dagster_job",
            cron_schedule="* * * * *"
        ) 
    ]
)
