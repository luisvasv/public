{% docs __overview__ %}
![header_1](https://i.postimg.cc/yxg5LVWN/hadder-1.png)
![header_2](https://i.postimg.cc/rwHRd4jX/dbt-dagster-speakers.png)

## **TLDR**

DBT (Data Build Tool) is an open-source software tool that enables data analysts and engineers to transform and model data in the data warehouse. It simplifies the ETL process by focusing on the ‘T’ — transformation — and integrates seamlessly with modern cloud-based data platforms.

![architecture](https://miro.medium.com/v2/resize:fit:720/format:webp/1*aJY44qWnpxtZqAHdKiU9fQ.png)

## CORE PRINCIPLES OF DBT

* Data Warehouse-Centric
* SQL as the DSL
* Git-based Version Control
* Model Dependencies
* Data Testing
* Jinja Templating
* CLI and API Integration
* Configurations & Hooks
* Extensibility with Adapters



## CONCEPTS USED ON THIS DEMO

* [Custom schemas](https://docs.getdbt.com/docs/build/custom-schemas)
* [Model Config](https://docs.getdbt.com/reference/model-configs)
* [Profiles](https://docs.getdbt.com/docs/core/connect-data-platform/profiles.yml)
* [Custom Docs](https://docs.getdbt.com/docs/collaborate/documentation)
* [Hooks & Operations](https://docs.getdbt.com/docs/build/hooks-operations)
* [Tags](https://docs.getdbt.com/reference/resource-configs/tags)
* [Materializations](https://docs.getdbt.com/docs/build/materializations)
* [Data Sources](https://docs.getdbt.com/docs/build/sources)
* [Packages](https://docs.getdbt.com/docs/build/packages)
* [Tests](https://docs.getdbt.com/docs/build/data-tests)
* [dbt_utils](https://hub.getdbt.com/dbt-labs/dbt_utils/latest/)

For more details, visit: [SOURCE: Understanding DBT (Data Build Tool): An Introduction](https://m.mage.ai/understanding-dbt-data-build-tool-an-introduction-751112595dc7)
{% enddocs %}

