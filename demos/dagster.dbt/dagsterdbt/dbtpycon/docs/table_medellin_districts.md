{% docs table_medellin_districts %}

Table containing information about the neighborhoods and zones of the municipality of Medellín to be cross-referenced with stg_medellin_events.point. The zones of Medellín are distributions used to group neighborhoods, as shown in the image:
![base events](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhwcGZyW99kTvlet6GlPr47W1Aw3ITtu76v8_XARu1_Qnogb54ZcIXP3TFQqYVHjSWHpBxsEKU6VinR8Vf-gV3xUt1hucJHSWeALwe_8maMfoa-AyyvkgjMDNIkwTsV8vDY3XxwD5u51bat/s400/-comunas_de_medellin-svg.png)

{% enddocs %}