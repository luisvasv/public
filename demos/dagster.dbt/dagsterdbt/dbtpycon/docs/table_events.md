{% docs table_events %}

This table is used to simulate real-time events for the software demo. Initially, the data is in the following format:
![base events](https://i.postimg.cc/J4S4qn69/base-events.png)

After transformations, it is stored in a columnar format.
{% enddocs %}