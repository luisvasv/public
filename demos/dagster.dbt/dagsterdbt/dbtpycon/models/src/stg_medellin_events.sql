{%- set partition_date = modules.datetime.date.today().strftime("%d%m%Y") -%}

SELECT 
    ST_POINT(ev.longitude, ev.latitude) point,
    ev.customer_id,
    UPPER(ct.name) customer_name,
    ev.employee_id,
    UPPER(ep.name) employee_name,
    ev.quantity_products,
    ev.event_date,
    ev.partition_date,
    ev.order_id

FROM {{source('base', 'events')}} ev
INNER JOIN {{source('constituents', 'customers')}} ct 
    ON ev.customer_id = ct.customer_id
INNER JOIN {{source('constituents', 'employees')}} ep 
    ON ev.employee_id = ep.employee_id
-- WHERE ev.partition_date = '30052024'
--WHERE ev.partition_date = '{{ partition_date }}'







