{{
  config(
    sort="me.partition_date",
    dist="me.order_id",
  )
}}
{%- set partition_date = modules.datetime.date.today().strftime("%d%m%Y") -%}

SELECT 
    me.partition_date,
    me.order_id,
    me.event_date,
    md.neighborhood,
    md.district,
    me.customer_id,
    me.customer_name,
    me.employee_id,
    me.employee_name,
    me.quantity_products,
    EXTRACT(DAY FROM TO_TIMESTAMP(me.event_date, 'DD/MM/YYYY HH24:MI:SS')) AS day,
    EXTRACT(MONTH FROM TO_TIMESTAMP(me.event_date, 'DD/MM/YYYY HH24:MI:SS')) AS month,
    EXTRACT(YEAR FROM TO_TIMESTAMP(me.event_date, 'DD/MM/YYYY HH24:MI:SS')) AS year,
    EXTRACT(HOUR FROM TO_TIMESTAMP(me.event_date, 'DD/MM/YYYY HH24:MI:SS')) AS hour_day,
    EXTRACT(MINUTE FROM TO_TIMESTAMP(me.event_date, 'DD/MM/YYYY HH24:MI:SS')) AS minute,
    EXTRACT(SECOND FROM TO_TIMESTAMP(me.event_date, 'DD/MM/YYYY HH24:MI:SS')) AS second
FROM {{ ref('dim_medellin_districts') }}  md
INNER JOIN {{ ref('stg_medellin_events') }} me
    ON ST_CONTAINS(md.geometry,  me.point)
--WHERE me.partition_date = '{{ partition_date }}'