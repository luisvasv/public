{{
    config(
        materialized='incremental',
        unique_key='partition_date',
        incremental_strategy='merge'
    )
}}

SELECT 
    partition_date,
    hour_day,
    max_top
FROM (
    SELECT 
        partition_date,
        hour_day,
        top AS max_top,
        ROW_NUMBER() OVER (PARTITION BY partition_date ORDER BY top DESC) AS row_num
    FROM {{ ref('kpi_sales_per_hour') }}
) tmp
WHERE row_num = 1