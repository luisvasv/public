{%- set partition_date = modules.datetime.date.today().strftime("%d%m%Y") -%}

{{ config(
    materialized='incremental',
    unique_key="partition_date",
    incremental_strategy="delete+insert"
) }}

-- CTE para calcular los totales por día y distrito
WITH products_per_day AS (
    SELECT 
        partition_date,
        district,
        SUM(quantity_products) top 
    FROM {{ ref('orders') }}
        GROUP BY partition_date, district
)
SELECT * FROM products_per_day