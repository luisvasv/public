{%- set partition_date = modules.datetime.date.today().strftime("%d%m%Y") -%}
{{
    config(
        materialized='incremental',
        unique_key="partition_date",
        incremental_strategy="delete+insert"
    )
}}
WITH kpi_sales_per_hour AS (
    SELECT 
    partition_date,
    hour_day,
    SUM(quantity_products) top
FROM {{ ref('orders') }}
--WHERE partition_date = '{{ partition_date }}' 
GROUP BY partition_date, hour_day
)

SELECT * 
FROM kpi_sales_per_hour


{% if is_incremental() %}
WHERE  (partition_date, hour_day) NOT IN (
    SELECT partition_date, hour_day FROM {{ this }}
)
{% endif %}
