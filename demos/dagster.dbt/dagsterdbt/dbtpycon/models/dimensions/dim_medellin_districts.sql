


SELECT codigo neighborhood,
    identificacion district,
    ST_GEOGRAPHYFROMWKB(geometry) geometry
FROM {{source('utils', 'polygon_neigborhoods_medellin')}}
WHERE codigo NOT IN ('SN01', 'SN02')