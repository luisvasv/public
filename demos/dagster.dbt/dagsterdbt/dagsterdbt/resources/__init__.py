import yaml
from typing import Any
from dagster import EnvVar
from pydantic import PrivateAttr
from yaml.loader import SafeLoader
from dagster_dbt import DbtCliResource
from dagster import ConfigurableResource
from ..assets.constants import DBT_DIRECTORY
from dagster_snowflake import SnowflakeResource

"""Allows defining the different resources that will be used in the process.
"""

# snowflake config
snowflake = SnowflakeResource(
    account=EnvVar("SNOWFLAKE_ACCOUNT"),
    user=EnvVar("SNOWFLAKE_USER"),
    password=EnvVar("SNOWFLAKE_PASSWORD"),
    warehouse=EnvVar("SNOWFLAKE_WAREHOUSE"),
    schema=EnvVar("SNOWFLAKE_SCHEMA"),
    role=EnvVar("SNOWFLAKE_ROLE"),
)


# resource for own cofig
class AssetsConfig(ConfigurableResource):
    path: str
    __content = PrivateAttr()

    def __init__(self, **data) -> None:
        super().__init__(**data)
        self.__set_config()

    def __set_config(self) -> None:
        with open(self.path) as file:
            self.__content = yaml.load(file, Loader=SafeLoader)

    def get(self, key: str) -> Any:
        return self.__content.get(key)


# dbt metadata
dbt_resource = DbtCliResource(
    project_dir=DBT_DIRECTORY,
)
