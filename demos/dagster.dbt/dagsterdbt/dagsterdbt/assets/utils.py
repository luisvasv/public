

from dagster import AssetKey
from typing import Any, Mapping, Optional
from dagster_dbt import DagsterDbtTranslator


class CustomDagsterDbtTranslator(DagsterDbtTranslator):
    """Class that interprets DBT assets and connects them with Dagster assets to enable seamless integration.
    """
    def get_asset_key(self, dbt_resource_props):
        """method that allows analyzing and connecting DBT assets with Dagster assets to achieve full lineage.

        :param dbt_resource_props: _description_
        :type dbt_resource_props: _type_
        :return: _description_
        :rtype: _type_
        """
        resource_type = dbt_resource_props["resource_type"]
        name = dbt_resource_props["name"]
        if resource_type == "source":
            if name == "events":
                return AssetKey(f"read_streaming_{name}")
            elif name == "employees":
                return AssetKey(f"insert_{name}_false")
            elif name == "customers":
                return AssetKey("insert_customer_false")
            elif name == "polygon_neigborhoods_medellin":
                return AssetKey("insert_med_neighborhoods_polygon_false")
        else:
            return super().get_asset_key(dbt_resource_props)

    def get_metadata(self, dbt_node_info: Mapping[str, Any]) -> Mapping[str, Any]:
        """get metadata from dbt,

        :param dbt_node_info: dbt mapping object, available keys:
            database
            schema
            name
            resource_type
            package_name
            path
            original_file_path
            unique_id
            fqn
            alias
            checksum
            config
            tags
            description
            columns
            meta
            group
            docs
            patch_path
            build_path
            unrendered_config
            created_at
            relation_name
            raw_code
            language
            refs
            sources
            metrics
            depends_on
            compiled_path
            compiled
            compiled_code
            extra_ctes_injected
            extra_ctes
            contract
            access
            constraints
            version
            latest_version
            deprecation_date
        :type dbt_node_info: Mapping[str, Any]
        :return: _description_
        :rtype: Mapping[str, Any]
        """
        return {
            "columns": dbt_node_info["columns"],
            "sources": dbt_node_info["sources"],
            "description": dbt_node_info["description"],
        }

    def get_group_name(
        self, dbt_resource_props: Mapping[str, Any]
    ) -> Optional[str]:
        return "metrics_engine"
