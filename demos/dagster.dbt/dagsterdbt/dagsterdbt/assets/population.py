import fsspec
import pandas as pd
from dagster import (
    asset,
    AssetIn,
    MaterializeResult,
    MetadataValue,
    ConfigurableResource,
    multi_asset,
    AssetOut,
    Output
)
import geopandas as gpd
from dagster_snowflake import SnowflakeResource
from snowflake.connector.pandas_tools import write_pandas


@asset(
    deps=["test_connection"],
    compute_kind="snowflake",
    group_name="populate"
)
def exits_customers_db(snowflake: SnowflakeResource, config: ConfigurableResource) -> bool:
    """validate if the users table exists, if it does not, will be created

    :param snowflake: nowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: if table exists, bool value
    :rtype: bool
    """
    table_exists: bool = False
    table_name: str = config.get("tables")["customers"]["name"]
    schema_name: str = config.get("tables")["customers"]["schema"]
    with snowflake.get_connection() as conn:
        table_exists = conn.cursor().execute(
            config.get("sql")["table_exists"].format(
                schema_name,
                table_name
            )
        ).fetchone()[0]
    return table_exists


@multi_asset(
    ins={"exits_customers_db": AssetIn("exits_customers_db")},
    outs={
        "preprare_customer_insertion": AssetOut(
            is_required=False,
            group_name="populate",
            description="requires users population"
        ),
        "insert_customer_false": AssetOut(
            is_required=False,
            group_name="no_actions",
            description="no required actions on snowflake"
        ),
    }
)
def customers_conditional(exits_customers_db: bool):
    """Conditional to determine if resources need to be created in Snowflake or not.
    :param exits_customers_db: _description_
    :type exits_customers_db: bool
    :yield: dagster ouput
    :rtype: Output
    """
    if exits_customers_db:
        yield Output(exits_customers_db, "insert_customer_false", metadata={"table_status": "existist"})
    else:
        yield Output(exits_customers_db, "preprare_customer_insertion", metadata={"table_status": "new"})


@asset(
    deps=["preprare_customer_insertion"],
    group_name="populate",
    compute_kind="pandas",

)
def get_customer_data(config: ConfigurableResource) -> pd.DataFrame:
    """Allows retrieving information from the remotely stored files of customers.

    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: curstomers data
    :rtype: pd.DataFrame
    """
    return pd.read_parquet(config.get("files")["customers"])


@asset(
    group_name="populate",
    compute_kind="snowflake",
    ins={"customer_data": AssetIn("get_customer_data")}

)
def create_insert_customer_data(
    snowflake: SnowflakeResource,
    config: ConfigurableResource,
    customer_data: pd.DataFrame
):
    """Allows inserting curstomers data into Snowflake.

    :param snowflake: snowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :param customer_data: data to be inserted
    :type customer_data: pd.DataFrame
    :return: metadata info
    :rtype: MaterializeResult
    """
    db_name: str = config.get("tables")["customers"]["database"]
    table_name: str = config.get("tables")["customers"]["name"]
    schema_name: str = config.get("tables")["customers"]["schema"]

    rows_inserted: int = 0
    with snowflake.get_connection() as conn:
        _, _, rows_inserted, _ = write_pandas(
            conn,
            customer_data,
            table_name=table_name,
            database=db_name,
            schema=schema_name,
            auto_create_table=True,
            overwrite=True,
            quote_identifiers=False,
        )

    return MaterializeResult(
        metadata={
            'number of records': MetadataValue.int(rows_inserted)
        }
    )


@asset(
    deps=["test_connection"],
    compute_kind="snowflake",
    group_name="populate"
)
def exits_employees_db(snowflake: SnowflakeResource, config: ConfigurableResource) -> bool:
    """validate if the users table exists, if it does not, will be created

    :param snowflake: nowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: if table exists, bool value
    :rtype: bool
    """
    table_exists: bool = False
    table_name: str = config.get("tables")["employees"]["name"]
    schema_name: str = config.get("tables")["employees"]["schema"]
    with snowflake.get_connection() as conn:
        table_exists = conn.cursor().execute(
            config.get("sql")["table_exists"].format(
                schema_name,
                table_name
            )
        ).fetchone()[0]
    return table_exists


@multi_asset(
    ins={"exits_employees_db": AssetIn("exits_employees_db")},
    outs={
        "preprare_employees_insertion": AssetOut(
            is_required=False,
            group_name="populate",
            description="requires users population"),
        "insert_employees_false": AssetOut(
            is_required=False,
            group_name="no_actions",
            description="no required actions on snowflake"
        ),
    }
)
def employees_conditional(exits_employees_db: bool):
    """Conditional to determine if resources need to be created in Snowflake or not.

    :param exits_employees_db: _description_
    :type exits_employees_db: bool
    :yield: dagster ouput
    :rtype: Output
    """
    if exits_employees_db:
        yield Output(exits_employees_db, "insert_employees_false", metadata={"table_status": "existist"})
    else:
        yield Output(exits_employees_db, "preprare_employees_insertion", metadata={"table_status": "new"})


@asset(
    deps=["preprare_employees_insertion"],
    group_name="populate",
    compute_kind="pandas",

)
def get_employees_data(config: ConfigurableResource) -> pd.DataFrame:
    """Allows retrieving information from the remotely stored files of employees.

    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: employees data
    :rtype: pd.DataFrame
    """
    return pd.read_parquet(config.get("files")["employees"])


@asset(
    group_name="populate",
    compute_kind="snowflake",
    ins={"customer_data": AssetIn("get_employees_data")}

)
def create_insert_employees_data(
    snowflake: SnowflakeResource,
    config: ConfigurableResource,
    customer_data: pd.DataFrame
):
    """Allows inserting empoyes data into Snowflake.

    :param snowflake: snowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :param customer_data: data to be inserted
    :type customer_data: pd.DataFrame
    :return: metadata info
    :rtype: MaterializeResult
    """
    db_name: str = config.get("tables")["employees"]["database"]
    table_name: str = config.get("tables")["employees"]["name"]
    schema_name: str = config.get("tables")["employees"]["schema"]

    rows_inserted: int = 0
    with snowflake.get_connection() as conn:
        _, _, rows_inserted, _ = write_pandas(
            conn,
            customer_data,
            table_name=table_name,
            database=db_name,
            schema=schema_name,
            auto_create_table=True,
            overwrite=True,
            quote_identifiers=False,
        )

    return MaterializeResult(
        metadata={
            'number of records': MetadataValue.int(rows_inserted)
        }
    )


@asset(
    deps=["test_connection"],
    compute_kind="snowflake",
    group_name="populate"
)
def exits_med_city_polygon_db(snowflake: SnowflakeResource, config: ConfigurableResource) -> bool:
    """ validate if the users table exists, if it does not, will be created

    :param snowflake: nowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: if table exists, bool value
    :rtype: bool
    """
    table_exists: bool = False
    table_name: str = config.get("tables")["med_city_polygon"]["name"]
    schema_name: str = config.get("tables")["med_city_polygon"]["schema"]
    with snowflake.get_connection() as conn:
        table_exists = conn.cursor().execute(
            config.get("sql")["table_exists"].format(
                schema_name,
                table_name
            )
        ).fetchone()[0]
    return table_exists


@multi_asset(
    ins={"exits_med_city_polygon_db": AssetIn("exits_med_city_polygon_db")},
    outs={
        "preprare_med_city_polygon_insertion": AssetOut(
            is_required=False,
            group_name="populate",
            description="requires users population"
        ),
        "insert_med_city_polygon_false": AssetOut(
            is_required=False,
            group_name="no_actions",
            description="no required actions on snowflake"
        ),
    }
)
def med_city_polygon_conditional(exits_med_city_polygon_db: bool):
    """Conditional to determine if resources need to be created in Snowflake or not.

    :param exits_med_city_polygon_db: _description_
    :type exits_med_city_polygon_db: bool
    :yield: dagster ouput
    :rtype: Output
    """
    if exits_med_city_polygon_db:
        yield Output(exits_med_city_polygon_db, "insert_med_city_polygon_false", metadata={"table_status": "existist"})
    else:
        yield Output(exits_med_city_polygon_db, "preprare_med_city_polygon_insertion", metadata={"table_status": "new"})


@asset(
    deps=["preprare_med_city_polygon_insertion"],
    group_name="populate",
    compute_kind="geopandas",

)
def get_med_city_polygon_data(config: ConfigurableResource) -> gpd.GeoDataFrame:
    """Allows retrieving information from the remotely stored files of medellin.

    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: polygons data
    :rtype: gpd.GeoDataFrame
    """
    with fsspec.open(config.get("files")["med_city_polygon"], mode='rb') as file:
        return gpd.read_parquet(file)


@asset(
    group_name="populate",
    compute_kind="snowflake",
    ins={"customer_data": AssetIn("get_med_city_polygon_data")}

)
def create_insert_med_city_polygon_data(
    snowflake: SnowflakeResource, config:
        ConfigurableResource,
        customer_data: pd.DataFrame
):
    """Allows inserting the medellin polygons into Snowflake.

    :param snowflake: snowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :param customer_data: data to be inserted
    :type customer_data: pd.DataFrame
    :return: metadata info
    :rtype: MaterializeResult
    """
    db_name: str = config.get("tables")["med_city_polygon"]["database"]
    table_name: str = config.get("tables")["med_city_polygon"]["name"]
    schema_name: str = config.get("tables")["med_city_polygon"]["schema"]

    rows_inserted: int = 0
    with snowflake.get_connection() as conn:
        _, _, rows_inserted, _ = write_pandas(
            conn,
            customer_data,
            table_name=table_name,
            database=db_name,
            schema=schema_name,
            auto_create_table=True,
            overwrite=True,
            quote_identifiers=False,
        )

    return MaterializeResult(
        metadata={
            'number of records': MetadataValue.int(rows_inserted)
        }
    )


@asset(
    deps=["test_connection"],
    compute_kind="snowflake",
    group_name="populate"
)
def exits_med_neighborhoods_polygon_db(snowflake: SnowflakeResource, config: ConfigurableResource) -> bool:
    """ validate if the users table exists, if it does not, will be created

    :param snowflake: nowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: if table exists, bool value
    :rtype: bool
    """
    table_exists: bool = False
    table_name: str = config.get("tables")["med_neighborhoods_polygon"]["name"]
    schema_name: str = config.get("tables")["med_neighborhoods_polygon"]["schema"]
    with snowflake.get_connection() as conn:
        table_exists = conn.cursor().execute(
            config.get("sql")["table_exists"].format(
                schema_name,
                table_name
            )
        ).fetchone()[0]
    return table_exists


@multi_asset(
    ins={"exits_med_neighborhoods_polygon_db": AssetIn("exits_med_neighborhoods_polygon_db")},
    outs={
        "preprare_med_neighborhoods_polygon_insertion": AssetOut(
            is_required=False, group_name="populate",
            description="requires users population"
        ),
        "insert_med_neighborhoods_polygon_false": AssetOut(
            is_required=False, group_name="no_actions",
            description="no required actions on snowflake"
        ),
    }
)
def med_neighborhoods_polygon_conditional(exits_med_neighborhoods_polygon_db: bool):
    """Conditional to determine if resources need to be created in Snowflake or not.

    :param exits_med_neighborhoods_polygon_db: validate if exists or not
    :type exits_med_neighborhoods_polygon_db: bool
    :yield: dagster ouput
    :rtype: Output
    """
    if exits_med_neighborhoods_polygon_db:
        yield Output(
            exits_med_neighborhoods_polygon_db,
            "insert_med_neighborhoods_polygon_false",
            metadata={"table_status": "existist"}
        )
    else:
        yield Output(
            exits_med_neighborhoods_polygon_db,
            "preprare_med_neighborhoods_polygon_insertion",
            metadata={"table_status": "new"}
        )


@asset(
    deps=["preprare_med_neighborhoods_polygon_insertion"],
    group_name="populate",
    compute_kind="geopandas",

)
def get_med_neighborhoods_polygon_data(config: ConfigurableResource) -> gpd.GeoDataFrame:
    """Allows retrieving information from the remotely stored files of neighborhoods.

    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: polygons data
    :rtype: gpd.GeoDataFrame
    """
    with fsspec.open(config.get("files")["med_neighborhoods_polygon"], mode='rb') as file:
        return gpd.read_parquet(file)


@asset(
    group_name="populate",
    compute_kind="snowflake",
    ins={"customer_data": AssetIn("get_med_neighborhoods_polygon_data")}

)
def create_insert_med_neighborhoods_polygon_data(
    snowflake: SnowflakeResource,
    config: ConfigurableResource,
    customer_data: pd.DataFrame
) -> MaterializeResult:
    """Allows inserting the neighborhood polygons into Snowflake.

    :param snowflake: snowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :param customer_data: data to be inserted
    :type customer_data: pd.DataFrame
    :return: metadata info
    :rtype: MaterializeResult
    """
    db_name: str = config.get("tables")["med_neighborhoods_polygon"]["database"]
    table_name: str = config.get("tables")["med_neighborhoods_polygon"]["name"]
    schema_name: str = config.get("tables")["med_neighborhoods_polygon"]["schema"]

    rows_inserted: int = 0
    with snowflake.get_connection() as conn:
        _, _, rows_inserted, _ = write_pandas(
            conn,
            customer_data,
            table_name=table_name,
            database=db_name,
            schema=schema_name,
            auto_create_table=True,
            overwrite=True,
            quote_identifiers=False,
        )

    return MaterializeResult(
        metadata={
            'number of records': MetadataValue.int(rows_inserted)
        }
    )
