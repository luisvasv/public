from pathlib import Path


# dbt project path
DBT_DIRECTORY = Path(__file__).joinpath("..", "..", "..", "dbtpycon").resolve()
