import os
import json
import boto3
import pandas as pd
from datetime import datetime
from .constants import DBT_DIRECTORY
from dagster_snowflake import SnowflakeResource
from .utils import CustomDagsterDbtTranslator
from snowflake.connector.pandas_tools import write_pandas
from dagster import AssetExecutionContext, asset, ConfigurableResource, MaterializeResult, MetadataValue
from dagster_dbt import dbt_assets, DbtCliResource

# get dbt manifest
dbt_manifest_path = os.path.join(DBT_DIRECTORY, "target", "manifest.json")


@dbt_assets(
    manifest=dbt_manifest_path,
    dagster_dbt_translator=CustomDagsterDbtTranslator(),
)
def dbt_snowflake(context: AssetExecutionContext, dbt: DbtCliResource):
    """[IMPORTANT] asset to run dbt

    :param context: dagster context for jobs
    :type context: AssetExecutionContext
    :param dbt: dbt config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type dbt: DbtCliResource
    :yield: stream output
    :rtype: dbt output
    """
    yield from dbt.cli(["run"], context=context).stream()


@asset(
    deps=[
        "test_connection",
        "insert_employees_false",
        "insert_customer_false",
        "insert_med_neighborhoods_polygon_false"
    ],
    compute_kind="aws",
    group_name="producer"
)
def read_streaming_events(snowflake: SnowflakeResource, config: ConfigurableResource) -> MaterializeResult:
    """read messages from SQS (localstack)

    :param snowflake: snowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: metadata for the job
    :rtype: MaterializeResult
    """

    # total events saved
    total_events: int = 0

    # database info
    db_name: str = config.get("tables")["events"]["database"]
    table_name: str = config.get("tables")["events"]["name"]
    schema_name: str = config.get("tables")["events"]["schema"]

    # set aws boto3 client
    sqs_client: boto3.client = boto3.client(
        service_name='sqs',
        aws_access_key_id='test',
        aws_secret_access_key='test',
        endpoint_url=os.getenv("AWS_CORE_END_POINT"),
    )

    # get SQS url
    queue_url: str = sqs_client.get_queue_url(
        QueueName=os.getenv("AWS_SQS_EVENTS")
    )['QueueUrl']

    # read messages
    response = sqs_client.receive_message(
        QueueUrl=queue_url,
        MaxNumberOfMessages=10,  # max messages to receive
        WaitTimeSeconds=20  # wait time to receive message
    )

    # check if there are message
    if 'Messages' in response:
        for message in [message for message in response['Messages'] if len(message['Body']) > 0]:
            events = pd.DataFrame([json.loads(message['Body'])])
            events['date'] = pd.to_datetime(events['date'], format='%d/%m/%Y %H:%M:%S')
            events['event_date'] = events['date'].dt.strftime('%d/%m/%Y %H:%M:%S')
            events['employee_id'] = events['employee_id'].astype(int)
            events['customer_id'] = events['customer_id'].astype(int)
            del events['date']
            events['partition_date'] = datetime.now().strftime("%d%m%Y")

            # insert data on snowflake
            with snowflake.get_connection() as conn:
                _, _, rows_inserted, _ = write_pandas(
                    conn,
                    events,
                    table_name=table_name,
                    database=db_name,
                    schema=schema_name,
                    auto_create_table=True,
                    overwrite=False,
                    quote_identifiers=False,
                    chunk_size=300
                )
                total_events = total_events + rows_inserted
                print(f" order id {events['order_id']} --[STEP|#2] ETL done")

            # delete events from SQS
            receipt_handle = message['ReceiptHandle']
            sqs_client.delete_message(
                QueueUrl=queue_url,
                ReceiptHandle=receipt_handle
            )
            print(f"message processed successfully : --> {message['Body']}")
    else:
        print("there is no message into queue")

    return MaterializeResult(
        metadata={
            'number of records': MetadataValue.int(total_events)
        }
    )
