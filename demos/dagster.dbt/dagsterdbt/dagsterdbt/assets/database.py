import os
from dagster_snowflake import SnowflakeResource
from dagster import asset, AssetExecutionContext, MaterializeResult, ConfigurableResource


@asset(
    compute_kind="Python",
    group_name="transversal"
)
def test_connection(
    context: AssetExecutionContext,
    snowflake: SnowflakeResource,
    config: ConfigurableResource
) -> MaterializeResult:
    """test connectio with snowflake

    :param context: dagster context for jobs
    :type context: AssetExecutionContext
    :param snowflake: snowflake connection, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type snowflake: SnowflakeResource
    :param config: own config, see dagsterdbt.__init__ and dagsterdbt.resources.__init__
    :type config: ConfigurableResource
    :return: metadata info
    :rtype: MaterializeResult
    """
    version: str = ""
    msm: str = """
    It's a valid connection:
    WAREHOUSE --> {}
    USER      --> {}
    ROLE      --> {}
    """.format(
        os.getenv("SNOWFLAKE_WAREHOUSE"),
        os.getenv("SNOWFLAKE_USER"),
        os.getenv("SNOWFLAKE_ROLE")
    )

    with snowflake.get_connection() as conn:
        version = conn.cursor().execute(config.get("sql")["version"]).fetchone()[0]
        context.log.info(msm)
    return MaterializeResult(
        metadata={
            'connection_status': "OK",
            'connection_info': msm,
            'version': version
        }
    )
