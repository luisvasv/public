
import os
from .jobs import event_aws_dbt_job
from .assets import database, population, dbt
from .schedules import event_aws_dbt_schedule
from dagster import Definitions, load_assets_from_modules
from .resources import snowflake, AssetsConfig, dbt_resource

# main dir
__basedir__: str = os.path.dirname(os.path.abspath(__file__))

# config folder
__config__: str = os.path.join(__basedir__, "config", "app.yaml")

# assets
database = load_assets_from_modules([database])
fillout = load_assets_from_modules([population])
dbt_flows = load_assets_from_modules([dbt])

# jobs
all_jobs = [event_aws_dbt_job]

# schedules
all_schedules = [event_aws_dbt_schedule]

# definitions, for more reference see: https://docs.dagster.io/concepts/assets/software-defined-assets
defs = Definitions(
    assets=database + fillout + dbt_flows,
    resources={
        "snowflake": snowflake,
        "config": AssetsConfig(path=__config__),
        "dbt": dbt_resource,
    },
    jobs=all_jobs,
    schedules=all_schedules,
)
