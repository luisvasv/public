from dagster import ScheduleDefinition
from ..jobs import event_aws_dbt_job

"""Allows configuring the task to be scheduled every 3 minutes.
"""


event_aws_dbt_schedule = ScheduleDefinition(
    job=event_aws_dbt_job,
    cron_schedule="*/3 * * * *",  # every 3 minutes
)
