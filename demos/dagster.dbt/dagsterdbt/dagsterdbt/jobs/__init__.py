from dagster import define_asset_job
from ..assets.dbt import dbt_snowflake, read_streaming_events

"""Allows generating the job syntax as well as the assets it will use.
"""


event_aws_dbt_job = define_asset_job(
    name="job_pycon_2024_dagster_dbt",
    selection=[read_streaming_events, dbt_snowflake]
)
