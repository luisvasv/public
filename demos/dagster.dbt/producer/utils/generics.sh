#!/bin/bash

function validate_args(){
    error_message="you must send engine paramater, example --engine=localstack"
    if [ $# -eq 1 ]; then
        for arg in "$@"
        do
            case $arg in
                --engine=*)
                    engine="${arg#*=}"
                    ;;
                *)
                    echo "unknow args: $arg"
                    echo $error_message
                    exit 1
                    ;;
            esac
        done
    else
        echo $error_message
        exit 1 
    fi

    # check args
    if [ -z "$engine" ]; then
        echo $error_message
        exit 1
    fi

    # engine validator --engine == localstack
    if [ "$engine" != "localstack" ]; then
        echo "argument value for --engine must be: localstack or aws"
        exit 1
    fi

    echo $engine

}

function validate_number(){
    # checknumber format
    re='^[0-9]+$'
    if ! [[ $1 =~ $re ]]; then
        echo "[ERROR]: input must be a number."
        exit 1
    fi

}