#!/bin/python
import json
import uuid
import random
import pandas as pd
import geopandas as gpd
from producer.utils.generic import Utils
from producer.utils.args import parse_args_producer

if __name__ == "__main__":

    # get args
    args = parse_args_producer().parse_args()

    # read files
    polygon = gpd.read_parquet('producer/.localdata/50001.parquet')
    customers = pd.read_parquet("producer/.localdata/customers.parquet")
    employee = pd.read_parquet("producer/.localdata/employees.parquet")

    # random data
    hour: int = random.randint(args.start_date, args.end_date)
    minutes: int = random.randint(0, 59)
    seconds: int = random.randint(0, 59)
    quantity_products: int = random.randint(0, 25)
    date: str = f"{args.date} {hour:02d}:{minutes:02d}:{seconds:02d}"
    latitude, longitude = Utils.random_lat_lon_geometry(polygon.geometry.iloc[0])
    customer_id = random.choice(customers["customer_id"].tolist())
    employee_id = random.choice(employee["employee_id"].tolist())

    data = {
        "latitude": latitude,
        "longitude": longitude,
        "date": date,
        "customer_id": customer_id,
        "employee_id": employee_id,
        "quantity_products": quantity_products,
        "order_id": str(uuid.uuid4())
    }
    # print random data object
    print(json.dumps(data))
