#!/bin/bash

# load env variables
source ./producer/utils/generics.sh
source ./dagsterdbt/.env
source ~/miniconda3/etc/profile.d/conda.sh

## setting and validating stuff for producer


# [001] check localstack service
service=$(localstack status | grep -c "running")
if [ "$service" -eq 1 ]; then
    echo "[OK|INFO] - localstack servive is running"
else
    echo "[WARNING|INFO] localstack is not runnig, starting the service."
    localstack start &
    sleep 5
fi

# [002] activate .env environment
#source ./.venv/bin/activate
conda activate pycon24

# [003] install requirements
# pip install -r ./producer/requirements.txt --quiet
conda activate pycon24

# [004] validating engine
engine=$(validate_args $@)
if [ $? -ne 0 ]; then
    echo "$engine"
    exit 1
fi 

# [005] generating localstac events
echo "[OK|INFO] - creating queue topic..."

# crear topics 
if [ "$engine" == "localstack" ]; then
    awslocal --endpoint-url $AWS_CORE_END_POINT sqs create-queue --queue-name $AWS_SQS_EVENTS
fi
echo ""

# [006] verifying topic..
echo "[OK|INFO] - listing queue..."
if [ "$engine" == "localstack" ]; then
    echo ""
    awslocal --endpoint-url $AWS_CORE_END_POINT sqs list-queues
    echo ""
fi


# [007] request nro events
echo -n "input nro of events: "
read -e RECORDS

# validate date format
validate_number $RECORDS

# variables
START_DATE=8                    # start work day
END_DATE=17                     # end work day
SLEEP=5                        # sleep every 50 seconds

# [008] generate # events based on RECORDS and SLEEP variables 
#DATE=$(date +"%d/%m/%Y")
DATE="05/06/2024"
while true; do
    # nro random events by cicle
    RANDOM_NUM=$(( (RANDOM % RECORDS) + 1 ))
    sleep $SLEEP
    for ((index = 1; index <= RANDOM_NUM; index++)); do
        sleep 2
        echo "[OK|INFO] - generating message, day: $DATE, # $index ..."
        message=$(python -m producer.app --date $DATE --start_date $START_DATE --end_date $END_DATE)

        echo $message
        if [ "$engine" == "localstack" ]; then
            URL=$(awslocal --endpoint-url $AWS_CORE_END_POINT sqs list-queues | jq .QueueUrls  | jq --arg aws_sqs_events "$AWS_SQS_EVENTS" '.[] | select(contains($aws_sqs_events))')
            URL=$(echo "$URL" | sed 's/"//g')
            awslocal --endpoint-url $AWS_CORE_END_POINT sqs --queue-url send-message "$URL" --message-body "${message}"
        fi
        echo -e "[OK|INFO] - \nmessage # $index \ncontent ${message} \nsent to $AWS_SQS_EVENTS"
        echo ""
    done
done