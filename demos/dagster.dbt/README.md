<div style="display: flex; justify-content: space-around; align-items: center; width: 90%;">
  <img src="https://neuronamagazine.com/wp-content/uploads//2022/07/SoftServe_logo_new-681x205.png" alt="SoftServe Logo" style="height: 60px;">
  <img src="https://2024.pycon.co/images/pycon-logo.svg" alt="PyCon 2024 Logo" style="height: 30px;">
</div>

<img 
     src="https://i.postimg.cc/rwHRd4jX/dbt-dagster-speakers.png" 
     alt="reporte" 
     border="0"
/>
# DEMO CONTENT

## **TLDR**

DBT (Data Build Tool) is an open-source software tool that enables data analysts and engineers to transform and model data in the data warehouse. It simplifies the ETL process by focusing on the ‘T’ — transformation — and integrates seamlessly with modern cloud-based data platforms.

<img 
     src="https://miro.medium.com/v2/resize:fit:720/format:webp/1*aJY44qWnpxtZqAHdKiU9fQ.png" 
     alt="reporte" 
     border="0"
/>

## CORE PRINCIPLES OF DBT

* Data Warehouse-Centric
* SQL as the DSL
* Git-based Version Control
* Model Dependencies
* Data Testing
* Jinja Templating
* CLI and API Integration
* Configurations & Hooks
* Extensibility with Adapters


## CONCEPTS USED ON THIS DEMO

* [Custom schemas](https://docs.getdbt.com/docs/build/custom-schemas)
* [Model Config](https://docs.getdbt.com/reference/model-configs)
* [Profiles](https://docs.getdbt.com/docs/core/connect-data-platform/profiles.yml)
* [Custom Docs](https://docs.getdbt.com/docs/collaborate/documentation)
* [Hooks & Operations](https://docs.getdbt.com/docs/build/hooks-operations)
* [Tags](https://docs.getdbt.com/reference/resource-configs/tags)
* [Materializations](https://docs.getdbt.com/docs/build/materializations)

For more details, visit: [SOURCE: Understanding DBT (Data Build Tool): An Introduction](https://m.mage.ai/understanding-dbt-data-build-tool-an-introduction-751112595dc7)





DBT (Data Build Tool) is an open-source software tool that enables data analysts and engineers to transform and model data in the data warehouse. It simplifies the ETL process by focusing on the ‘T’ — transformation — and integrates seamlessly with modern cloud-based data platforms.


**dbt:** (*data build tool*): allows data teams to transform data using SQL, managing transformations as code. It facilitates versioning, automated testing, and documentation, making data management more efficient and organized.


**Dagster:** offers robust and modular orchestration of data workflows. Its ability to handle complexities and its integration with various systems make it indispensable for optimizing data processes.



<div class="alert alert-warning">
 <strong>⚠️IMPORTANT:</strong> If you wish to replicate this tutorial on your own, you must follow all the steps in the following section.
</div>

## 001 SET UP

### [001.1] SNOWFLAKE CONFIGURATION

The first thing you need to do is create an account on Snowflake (free-tier). To do this, visit the page https://www.snowflake.com/en/, click on **Start for Free**, and fill in the required fields and steps. At this point, it is important to note two things:

1. You should select:

    - **Snowflake Edition**: Standard
    - **Cloud Provider**: AWS 
    - **Zone**: US East
<img 
     src="https://i.postimg.cc/Rh1bh4xy/snowflake-advs.png" 
     alt="reporte" 
     border="0"
     width="160" height="200"
/>

It is recommended if for some reason snowflake ask for the warehouse name, you'll use the value `DASGTERDBT_WH` to avoid additional configurations.

**Note:** If you have used Snowflake before or do not want to use your personal accounts, I recommend the following website to generate temporary emails. You can also view the inbox at the same link: [10 Minute Email](https://10minemail.com/es/)

2. Once you have completed the data, you will receive a welcome email. It is very important that you copy the account value as shown in the following image:

![Preview](https://i.postimg.cc/XqyVF19p/snowflake.png)

[Insert image here if possible]  
The value enclosed in the red section should be copied and replaced in the following files:

- `dagsterdbt/.env`
- `dagsterdbt/dbtpycon/profiles.yml`

in the `SNOWFLAKE_ACCOUNT` variable.

3. If everything is correct, you will be able to authenticate in Snowflake. Then, you should execute the following commands to create the users that will be used in the demo:



```sql
-- Use an admin role
USE ROLE ACCOUNTADMIN;

-- Create the pycon role
CREATE ROLE IF NOT EXISTS pycon;
GRANT ROLE pycon TO ROLE ACCOUNTADMIN;

-- Create the default warehouse if necessary
CREATE WAREHOUSE IF NOT EXISTS DASGTERDBT_WH;
GRANT OPERATE ON WAREHOUSE DASGTERDBT_WH TO ROLE pycon;

-- Create the dagster user and assign to role
CREATE USER IF NOT EXISTS dagster
  PASSWORD='Dagster123**'
  LOGIN_NAME='dagster'
  MUST_CHANGE_PASSWORD=FALSE
  DEFAULT_WAREHOUSE='DASGTERDBT_WH'
  DEFAULT_ROLE='pycon'
  DEFAULT_NAMESPACE='softwater.raw'
  COMMENT='Dagster user used for add data and structures';
GRANT ROLE pycon to USER dagster;


-- Create the dbt user and assign to role
CREATE USER IF NOT EXISTS dbt
  PASSWORD='Dbt123**'
  LOGIN_NAME='dbt'
  MUST_CHANGE_PASSWORD=FALSE
  DEFAULT_WAREHOUSE='DASGTERDBT_WH'
  DEFAULT_ROLE='pycon'
  DEFAULT_NAMESPACE='softwater.raw'
  COMMENT='DBT user used for models';
GRANT ROLE pycon to USER dbt;

-- Create our database and schemas
CREATE DATABASE IF NOT EXISTS softwater;
CREATE SCHEMA IF NOT EXISTS softwater.raw;
CREATE SCHEMA IF NOT EXISTS softwater.silver;
CREATE SCHEMA IF NOT EXISTS softwater.gold;
CREATE SCHEMA IF NOT EXISTS softwater.dimensional;
CREATE SCHEMA IF NOT EXISTS softwater.users;

-- Set up permissions to role pycon
GRANT ALL ON WAREHOUSE DASGTERDBT_WH TO ROLE pycon; 
GRANT ALL ON DATABASE softwater to ROLE pycon;
GRANT ALL ON ALL SCHEMAS IN DATABASE softwater to ROLE pycon;
GRANT ALL ON FUTURE SCHEMAS IN DATABASE softwater to ROLE pycon;
GRANT ALL ON ALL TABLES IN SCHEMA softwater.raw to ROLE pycon;
GRANT ALL ON FUTURE TABLES IN SCHEMA softwater.raw to ROLE pycon;
GRANT ALL ON ALL TABLES IN SCHEMA softwater.silver to ROLE pycon;
GRANT ALL ON FUTURE TABLES IN SCHEMA softwater.silver to ROLE pycon;
GRANT ALL ON ALL TABLES IN SCHEMA softwater.gold to ROLE pycon;
GRANT ALL ON FUTURE TABLES IN SCHEMA softwater.gold to ROLE pycon;
GRANT ALL ON ALL TABLES IN SCHEMA softwater.dimensional to ROLE pycon;
GRANT ALL ON FUTURE TABLES IN SCHEMA softwater.dimensional to ROLE pycon;

GRANT ALL ON ALL TABLES IN SCHEMA softwater.users to ROLE pycon;
GRANT ALL ON FUTURE TABLES IN SCHEMA softwater.users to ROLE pycon;
```

### [001.2] INSTALL PYTHON LIBRARIES

1. For this step, it is recommended to have a virtual environment manager, either venv or conda. Once you have the environment ready, install the required libraries for the process. In the project's main folder, execute:


```
# Make sure you're working in Python version 3.10.x, then install the poetry environment
cd dagsterdbt && python -m poetry install 

```

2. Check the files in case something has changed from the initial configuration or if you need to set any missing values:

- `dagsterdbt/.env`
- `dagsterdbt/dbtpycon/profiles.yml`


### [001.3] SET UP DBT

To ensure that the integration with Dagster works correctly, you should perform the following actions:

1. Navigate to the dbt folder by executing the following command:
    ```sh
    cd dagsterdbt/dbtpycon/
    ```

2. Install the dependencies required by the project by executing:
    ```sh
    dbt deps
    ```

3. Verify if the configuration and connection with Snowflake are correctly set up by executing:
    ```sh
    dbt test
    ```

4. Generate the metadata so that it can be read and imported by Dagster by executing:
    ```sh
    dbt docs generate
    ```

5. [**OPTIONAL**] If you want to see the dbt documentation service as well as the documentation of the different components used for this POC, execute:
    ```sh
    dbt docs serve
    ```

    There you can interact and obtain more detailed information.

  cd dagsterdbt
   
### [001.4] SET UP VSCODE

VSCode (Visual Studio Code) is a free, open-source code editor developed by Microsoft, exalted for its support for debugging, syntax higlights, smart code completion, snippets, and Git integration.

[Download VSCODE](https://code.visualstudio.com/download)

Once VSCode have been downloaded, configure it as follows

Go to the Extensions section and install Tasks Exlorer.

<img 
     src="https://i.postimg.cc/V6K2BMrX/tasks.png" 
     alt="reporte" 
     border="0"
/>

After it is installed, go to Extensions > Tasks Exlorer > Extensions Settings > Enable Side Bar 
<img 
src="https://i.postimg.cc/05JLJ65K/config.png" 
     alt="reporte" 
     border="0"
/>

<img 
src="https://i.postimg.cc/xdCZj8FC/enabled.png" 
     alt="reporte" 
     border="0"
/>

Restart VSCODE, click on the Task Explorer icon and it should appear like this:

<img 
src="https://i.postimg.cc/0yyG7hVt/dbt-producer.png" 
     alt="reporte" 
     border="0"
/>

### [001.5] SET UP DOCKER

Docker is a platform that allows you to package, distribute, and run applications in containers. To run the demo, Docker must be downloaded and running. For more info visit:  https://www.docker.com/products/docker-desktop/


## 002 PLAYING WITH DAGSTER

To interact with Dagster, first activate your virtual environment (conda or virtualenv). You can then proceed with this demo in two ways:

## [002.1] Basic Approach

If you have no experience with Dagster or prefer something simpler and lighter, while also testing in developer mode, execute the following:

```bash
dagster dev -f dagsterstandalone/single.py
```

Open the following link in your browser: `http://127.0.0.1:3000`. You will see something like this:

<img 
src="https://i.postimg.cc/NjgHGwx8/stand-alone.png" 
     alt="reporte" 
     border="0"
/>
Click on `Materialize All` to start exploring safely.


### [002.2] DEMO DBT & DAGSTER

To interact with the demo, ensure you have close dagster service in the previous step if you've already executed it. Next, send events to the simulated SQS queue using the LocalStack service. Use the task explorer and run:


``[aws|local].event.producer``


Using the terminal:
```bash
bash producer/app.sh --engine=localstack
```

add the nro of events and then, execute in a new console: 
```bash
cd dagsterdbt && dagster dev
```

Open the following link in your browser: `http://127.0.0.1:3000`. You will see something like this:
<img 
src="https://i.postimg.cc/wxrSX6r2/full-demo.png" 
     alt="reporte" 
     border="0"
/>


Note: I recommend clicking on each task area to view logs and other advanced concepts that Dagster allows, such as `MaterializeResult` for automatically adding metadata and visualizing it.
